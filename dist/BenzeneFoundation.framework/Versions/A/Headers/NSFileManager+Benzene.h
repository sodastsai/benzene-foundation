//
//  NSFileManager+Benzene.h
//  BenzeneFoundation
//
//  Created by sodas on 4/5/14.
//  Copyright (c) 2014 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const NSFileManagerExtendedAttributeErrorDomain;

@interface NSFileManager (BFExtendedAttribute)

- (NSData *)extendedAttributeNamed:(NSString *)attrName ofItemAtPath:(NSString *)path
                             error:(NSError * __autoreleasing *)error;
- (BOOL)setValue:(NSData *)value extendedAttributeNamed:(NSString *)attrName ofItemAtPath:(NSString *)path
           error:(NSError * __autoreleasing *)error;
- (BOOL)removeExtendedAttributeNamed:(NSString *)attrName ofItemAtPath:(NSString *)path
                               error:(NSError * __autoreleasing *)error;
- (NSArray *)arrayWithExtendedAttributeNamesOfItemAtPath:(NSString *)path error:(NSError * __autoreleasing *)error;

@end
