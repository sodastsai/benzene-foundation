//
//  UIDevice+Benzene.h
//  BenzeneFoundation
//
//  Created by sodas on 12/20/13.
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (Benzene)

@property (nonatomic, strong, readonly) NSString *modelIdentifier;

@property (nonatomic, assign, readonly) NSInteger majorModelGeneration;
@property (nonatomic, assign, readonly) NSInteger minorModelGeneration;

@property (nonatomic, assign, readonly) BOOL isDevice;

@end
