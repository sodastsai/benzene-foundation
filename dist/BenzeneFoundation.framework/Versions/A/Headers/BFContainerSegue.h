//
//  BFContainerSegue.h
//  BenzeneFoundation
//
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BFContainerSegue;

/// Delegate of BFContainerSegue
@protocol BFContainerSegueDelegate <NSObject>

@optional

/**
 * Decide a container segue should embed a view controller or not
 *
 * @param segue              The container segue
 * @param viewController     The view controller to be embedded
 * @return                   The segue should perform embed action or not
 */
- (BOOL)containerSegue:(BFContainerSegue *)segue shouldEmbedViewController:(UIViewController *)viewController;

/**
 * A container segue has finish embedding a view controller or not
 *
 * @param segue              The container segue
 * @param viewController     The view controller which has been embedded
 */
- (void)containerSegue:(BFContainerSegue *)segue didEmbedViewController:(UIViewController *)viewController;

/**
 * Decide a container segue should add view of destination view controller to source view controller when embedding.
 *
 * @param segue     The container segue
 * @return          Whether the segue should add view to its container view when embedding view controller or not.
 */
- (BOOL)containerSegueShouldAddViewWhenEmbedding:(BFContainerSegue *)segue;

@end

/// A segue that used to embed destination view controller to source view controller
@interface BFContainerSegue : UIStoryboardSegue

/**
 * A view in source view controller that should hold the view of destination view controller
 * You may have to assign this property in -[UIViewController prepareForSegue:sender:]
 * in the implementation of your source view controller
 */
@property (nonatomic, strong) UIView *containerView;

/// Delegate of BFContainerSegue
@property (nonatomic, weak) id<BFContainerSegueDelegate> delegate;

@end

