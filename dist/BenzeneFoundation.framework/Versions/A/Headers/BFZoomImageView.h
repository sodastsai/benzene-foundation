//
//  BFZoomImageView.h
//  BenzeneFoundation
//
//  Copyright (c) 2012 Benzene. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BenzeneFoundation/BFDefines.h>

BF_EXTERN CGFloat const BFZoomImageViewDoubleTapZoomToMaxScale;

@interface BFZoomImageView : UIScrollView <UIScrollViewDelegate>

@property (nonatomic, strong, readonly) UIImageView *imageView;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong, readonly) UITapGestureRecognizer *doubleTapGestureRecognizer;
@property (nonatomic, assign, getter=isDoubleTapZoomEnabled) BOOL doubleTapZoomEnabled;
@property (nonatomic, assign) CGFloat doubleTapZoomScale;

@property (nonatomic, assign, getter=isZoomEnabled) BOOL zoomEnabled;

- (CGRect)visibleRectOfImage;

- (void)setImage:(UIImage *)image updateContentSize:(BOOL)updateContentSize;

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView;

- (CGFloat)fitScaleOfImage:(UIImage *)image;
- (void)zoomToScale:(CGFloat)scale atPoint:(CGPoint)center animated:(BOOL)animated;
- (void)zoomToActualSizeAtPoint:(CGPoint)center animated:(BOOL)animated;
- (void)zoomToMaxScaleAtPoint:(CGPoint)center animated:(BOOL)animated;
- (void)zoomToFitScaleWithAnimation:(BOOL)animated;

@end
