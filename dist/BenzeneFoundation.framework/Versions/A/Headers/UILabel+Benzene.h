//
//  UILabel+Benzene.h
//  BenzeneFoundation
//
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Benzene)

/**
 * Format this label with specified text attributes
 *
 * @param textAttributes     The text attributes to format this label
 * @discuss                  You can specify the font, text color, text shadow color, and text shadow offset for this
 *                           label in the text attributes dictionary, using the text attribute keys described in
 *                           ```NSString UIKit Additions Reference```.
 */
- (void)formatWithTextAttributes:(NSDictionary *)textAttributes;

@end
