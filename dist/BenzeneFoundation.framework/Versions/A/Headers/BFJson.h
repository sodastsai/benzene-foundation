//
//  BFJson.h
//  BenzeneFoundation
//
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BenzeneFoundation/BFDefines.h>

BF_STATIC_INLINE NSData *BFJsonData(id object, NSJSONWritingOptions options, NSError * __autoreleasing *error) {
    return object?[NSJSONSerialization dataWithJSONObject:object options:options error:error]:nil;
}
BF_STATIC_INLINE NSString *BFJsonString(id object, NSJSONWritingOptions options, NSError * __autoreleasing *error) {
    NSData *jsonData = BFJsonData(object, options, error);
    return jsonData?[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]:nil;
}
BF_STATIC_INLINE id BFObjectFromJsonData(NSData *jsonData, NSJSONReadingOptions options,
                                         NSError * __autoreleasing *error) {
    return jsonData?[NSJSONSerialization JSONObjectWithData:jsonData options:options error:error]:nil;
}
BF_STATIC_INLINE id BFObjectFromJsonString(NSString *jsonString, NSJSONReadingOptions options,
                                           NSError * __autoreleasing *error) {
    return jsonString?BFObjectFromJsonData([jsonString dataUsingEncoding:NSUTF8StringEncoding], options, error):nil;
}

/// This category provides functionality for NSString to objects using JSON.
@interface NSString (BFJson)

/**
 * Return the object encoded with JSON as the content of the receiver.
 *
 * @return     An object parsed as JSON string from the content of this receiver.
 */
- (id)jsonObject;
- (id)objectWithJSONValue;

@end

/// This category provides functionality for NSData to objects using JSON.
@interface NSData (BFJson)

/**
 * Return the object encoded with JSON as the content of the receiver.
 *
 * @return     An object parsed as JSON data from the content of this receiver.
 */
- (id)jsonObject;
- (id)objectWithJSONValue;

@end

/// This category provides functionality for NSArray to convert to/from JSON.
@interface NSArray (BFJson)

/**
 * Creates and returns an array using the keys and values found in the specified JSON string.
 *
 * @param jsonString     A JSON string that represents an array object.
 *
 * @return               A new array that contains the content represented by the JSON string.
 *                       `nil` if it is not a valid JSON string or it doesn't represent an array.
 */
+ (instancetype)arrayWithContentsOfJsonString:(NSString *)jsonString;

/**
 * Creates and returns an array using the keys and values found in the specified JSON data.
 *
 * @param jsonData     A JSON data that represents an array object.
 *
 * @return             A new array that contains the content represented by the JSON data.
 *                     `nil` if it is not a valid JSON data or it doesn't represent an array.
 */
+ (instancetype)arrayWithContentsOfJsonData:(NSData *)jsonData;

+ (instancetype)arrayWithContentsOfJsonFile:(NSString *)path;
- (BOOL)writeToJsonFile:(NSString *)path atomically:(BOOL)flag;

/**
 * Returns a string that represents the receiver in JSON format.
 *
 * @return     string with JSON representation of this receiver.
 */
- (NSString *)jsonString;
- (NSString *)stringWithJSONObject;

/**
 * Returns a data that represents the receiver in JSON format.
 *
 * @return     data with JSON representation of this receiver.
 */
- (NSData *)jsonData;
- (NSData *)dataWithJSONObject;

@end

/// This category provides functionality for NSDictionary to convert to/from JSON.
@interface NSDictionary (BFJson)

/**
 * Creates and returns a dictionary using the keys and values found in the specified JSON string.
 *
 * @param jsonString     A JSON string that represents a dict object.
 *
 * @return               A new dictionary that contains the content represented by the JSON string.
 *                       `nil` if it is not a valid JSON string or it doesn't represent a dictionary.
 */
+ (instancetype)dictionaryWithContentsOfJsonString:(NSString *)jsonString;

/**
 * Creates and returns a dictionary using the keys and values found in the specified JSON data.
 *
 * @param jsonData     A JSON data that represents a dict object.
 *
 * @return             A new dictionary that contains the content represented by the JSON data.
 *                     `nil` if it is not a valid JSON data or it doesn't represent a dictionary.
 */
+ (instancetype)dictionaryWithContentsOfJsonData:(NSData *)jsonData;

+ (instancetype)dictionaryWithContentsOfJsonFile:(NSString *)path;
- (BOOL)writeToJsonFile:(NSString *)path atomically:(BOOL)flag;

/**
 * Returns a string that represents the receiver in JSON format.
 *
 * @return     string with JSON representation of this receiver.
 */
- (NSString *)jsonString;
- (NSString *)stringWithJSONObject;

/**
 * Returns a data that represents the receiver in JSON format.
 *
 * @return     data with JSON representation of this receiver.
 */
- (NSData *)jsonData;
- (NSData *)dataWithJSONObject;

@end
