//
//  NSIndexPath+Benzene.h
//  BenzeneFoundation
//
//  Created by sodas on 3/12/14.
//  Copyright (c) 2014 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSIndexPath (Benzene)

+ (instancetype)indexPathWithIndexArray:(NSArray *)indexArray;
- (NSArray *)arrayWithIndexes;

@end
