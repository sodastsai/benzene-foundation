//
//  NSNumber+Benzene.h
//  BenzeneFoundation
//
//  Created by sodas on 2/27/14.
//  Copyright (c) 2014 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@interface NSNumber (BenzeneNumber)

- (CGFloat)CGFloatValue;

@end

@interface NSString (BenzeneNumber)

- (CGFloat)CGFloatValue;

@end

#define BFBoolNumber(condition) ((condition)?@YES:@NO)
