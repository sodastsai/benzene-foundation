//
//  BFURLEncoding.h
//  BenzeneFoundation
//
//  Created by sodas on 10/16/13.
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (BFURLEncoding)

/**
 * Return a URL-encoded representation of the receiver.
 *
 * @param encoding     the encoding used to do URL encoding
 *
 * @return    URL-encoded (percent-encoded) representation of the receiver.
 */
- (NSString *)urlEncodedStringUsingEncoding:(NSStringEncoding)encoding;

/**
 * Return a URL-decoded representation of the receiver.
 *
 * @param encoding     the encoding used to do URL decoding
 *
 * @return    URL-decoded (percent-decoded) representation of the receiver.
 */
- (NSString *)urlDecodedStringUsingEncoding:(NSStringEncoding)encoding;

@end

@interface NSDictionary (BFURLEncoding)

/**
 * Return a dictionary that represents URL-decoded value of the `urlEncodedString`
 *
 * @param urlEncodedString     The string that represents a dict in URL encoded form.
 *
 * @return                     String with URL-encoded representation of the receiver.
 */
+ (NSDictionary *)dictionaryWithContentsOfURLEncodedString:(NSString *)urlEncodedString;

/**
 * Return a string that represents URL-encoded value of the receiver
 *
 * @return     String with URL-encoded representation of the receiver.
 */
- (NSString *)stringWithURLEncodedValue;

@end
