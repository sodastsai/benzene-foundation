//
//  NSString+Benzene.h
//  BenzeneFoundation
//
//  Created by sodas on 3/6/14.
//  Copyright (c) 2014 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BenzeneFoundation/BFDefines.h>

@interface NSString (Benzene)

- (BOOL)containsString:(NSString *)subString;

- (instancetype)stringByLowercaseInRange:(NSRange)range;
- (instancetype)stringByUppercaseInRange:(NSRange)range;

@end

BF_STATIC_INLINE NSString *BFStringFromObject(id object) {
    if ([object isKindOfClass:[NSString class]]) {
        return object;
    } else if ([object isKindOfClass:[@YES class]]) {
        // Boolean
        return [object boolValue]?@"YES":@"NO";
    } else if ([object respondsToSelector:@selector(stringValue)]) {
        // Understand string value
        return [object stringValue];
    } else {
        return [object description];
    }
}
