//
//  BFDictionaryObjectHelper.h
//  BenzeneFoundation
//
//  Created by sodas on 2/24/14.
//  Copyright (c) 2014 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>

#define BFDictionaryObjectHelperApplyWithMutableDictionary(DICT) \
- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector { \
    return [super methodSignatureForSelector:aSelector]?: \
        [BFDictionaryObjectHelper methodSignatureForDynamicPropertySelector:aSelector inClass:[self class]]; } \
- (void)forwardInvocation:(NSInvocation *)anInvocation { \
    [BFDictionaryObjectHelper forwardInvocation:anInvocation dictionary:(DICT) inClass:[self class]]?: \
        [super forwardInvocation:anInvocation]; } \
- (void)setValue:(id)value forUndefinedKey:(NSString *)key { \
    [BFDictionaryObjectHelper setValue:value forUndefinedKey:key dictionary:(DICT) inClass:[self class]]?: \
        [super setValue:value forUndefinedKey:key]; } \
- (id)valueForUndefinedKey:(NSString *)key { \
    return [BFDictionaryObjectHelper valueForUndefinedKey:key dictionary:(DICT) inClass:[self class]]?: \
        [super valueForUndefinedKey:key]; }

@interface BFDictionaryObjectHelper : NSObject

+ (void)loadDynamicPropertyInfoForClass:(Class)classPtr;
+ (void)unloadDynamicPropertyInfoForClass:(Class)classPtr;

/*
 - (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector {
    return [super methodSignatureForSelector:aSelector]?:
        [BFDictionaryObjectHelper methodSignatureForDynamicPropertySelector:aSelector inClass:[self class]];
 }
 - (void)forwardInvocation:(NSInvocation *)anInvocation {
    [BFDictionaryObjectHelper forwardInvocation:anInvocation dictionary:self.dict inClass:[self class]]?:
        [super forwardInvocation:anInvocation];
 }
 - (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    [BFDictionaryObjectHelper setValue:value forUndefinedKey:key dictionary:self.dict inClass:[self class]]?:
        [super setValue:value forUndefinedKey:key];
 }
 - (id)valueForUndefinedKey:(NSString *)key {
    return [BFDictionaryObjectHelper valueForUndefinedKey:key dictionary:self.dict inClass:[self class]]?:
        [super valueForUndefinedKey:key];
 }
 */

+ (NSMethodSignature *)methodSignatureForDynamicPropertySelector:(SEL)selector inClass:(Class)classPtr;
+ (BOOL)forwardInvocation:(NSInvocation *)anInvocation dictionary:(NSMutableDictionary *)dictionary
                  inClass:(Class)classPtr;

+ (BOOL)setValue:(id)value forUndefinedKey:(NSString *)key dictionary:(NSMutableDictionary *)dictionary
         inClass:(Class)classPtr;
+ (id)valueForUndefinedKey:(NSString *)key dictionary:(NSMutableDictionary *)dictionary inClass:(Class)classPtr;

@end
