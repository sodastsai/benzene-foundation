//
//  BFDictObject.h
//  BenzeneFoundation
//
//  Created by sodas on 2/24/14.
//  Copyright (c) 2014 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BFDictObject : NSObject

@property (nonatomic, strong) NSMutableDictionary *dictionary;

- (instancetype)initWithDictionary:(NSMutableDictionary *)dictionary;

+ (NSArray *)arrayWithDictObjectsFromDictionaryArray:(NSArray *)dictArray;

@end
