//
//  UIAlertView+Benzene.h
//  BenzeneFoundation
//
//  Created by sodas on 12/11/13.
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (Benzene)

+ (instancetype)alertViewWithTitle:(NSString *)title
                           message:(NSString *)message
                 cancelButtonTitle:(NSString *)cancelButtonTitle
                 otherButtonTitles:(NSArray *)otherButtonTitles
                    dismissHandler:(void(^)(NSInteger dismissButtonIndex, UIAlertView *alertView))handler;

@end
