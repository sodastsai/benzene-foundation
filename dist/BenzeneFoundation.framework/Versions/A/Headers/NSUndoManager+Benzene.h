//
//  NSUndoManager+Benzene.h
//  BenzeneFoundation
//
//  Created by sodas on 4/18/14.
//  Copyright (c) 2014 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^BFUndoManagerBlock)(id target);

@interface NSUndoManager (Benzene)

- (void)registerUndoWithTarget:(id)target block:(BFUndoManagerBlock)block;

@end
