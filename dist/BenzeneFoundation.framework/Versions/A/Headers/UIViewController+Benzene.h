//
//  UIViewController+Benzene.h
//  BenzeneFoundation
//
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Benzene)

- (BOOL)isViewVisible;

@end

@interface UISplitViewController (Benzene)

- (UIViewController *)masterViewController;
- (UIViewController *)detailViewController;

@end
