//
//  BFAppSession.h
//  BenzeneFoundation
//
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BFAppSession : NSObject

@property (nonatomic, assign, readonly) NSTimeInterval backgroundTimeIntervalToRefreshSession;
@property (nonatomic, strong, readonly) NSDate *sessionStartDate;
@property (nonatomic, strong, readonly) NSDate *lastSessionEndDate;

+ (void)startAppSession;
+ (BFAppSession *)currentAppSession;

+ (void)startAppSessionWithRefreshTimeIntervalInBackground:(NSTimeInterval)refreshTimeInterval;
+ (BFAppSession *)appSessionWithRefreshTimeIntervalInBackground:(NSTimeInterval)refreshTimeInterval;

- (id)objectForKeyedSubscript:(id)key;
- (void)setObject:(id)object forKeyedSubscript:(id<NSCopying>)key;
- (id)objectForKey:(id)key;
- (void)setObject:(id)object forKey:(id<NSCopying>)key;

- (void)registerDefaultObject:(id)object forKey:(id<NSCopying>)key;
- (void)deregisterDefaultObjectForKey:(id<NSCopying>)key;
- (void)resetToDefaults;

- (NSDictionary *)defaultDictionary;
- (NSDictionary *)dictionary;

@end
