//
//  BFQueue.h
//  BenzeneFoundation
//
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BFQueueDelegate;

@interface BFQueue : NSObject

@property(nonatomic, weak) id<BFQueueDelegate> delegate;

@property(nonatomic, strong, readonly) NSArray *array;
@property(nonatomic, assign, getter=isEmpty, readonly) BOOL empty;
@property(nonatomic, assign, readonly) NSUInteger count;

+ (instancetype)queue;
+ (instancetype)queueWithCapacity:(NSUInteger)capacity;

- (void)enqueueObject:(id)object;
- (id)dequeue;
- (id)peek;
- (NSArray *)dequeueAllObjects;

- (void)compact;

- (BOOL)isEqualToQueue:(BFQueue *)queue;

- (NSEnumerator *)contentEnumerator;

@end

@protocol BFQueueDelegate <NSObject>

@optional

- (void)queue:(BFQueue *)queue didEnqueueObject:(id)object;
- (void)queue:(BFQueue *)queue didDequeueObject:(id)object;

@end
