//
//  BFStack.h
//  BenzeneFoundation
//
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BFStackDelegate;

@interface BFStack : NSObject

@property(nonatomic, weak) id<BFStackDelegate> delegate;

@property(nonatomic, strong, readonly) NSArray *array;
@property(nonatomic, assign, getter=isEmpty, readonly) BOOL empty;
@property(nonatomic, assign, readonly) NSUInteger count;

+ (instancetype)stack;
+ (instancetype)stackWithCapacity:(NSUInteger)capacity;

- (void)pushObject:(id)object;
- (id)pop;
- (id)peek;
- (NSArray *)popAllObjects;

- (void)compact;

- (BOOL)isEqualToStack:(BFStack *)stack;

- (NSEnumerator *)contentEnumerator;

@end

@protocol BFStackDelegate <NSObject>

@optional

- (void)stack:(BFStack *)stack didPushObject:(id)object;
- (void)stack:(BFStack *)stack didPopObject:(id)object;

@end
