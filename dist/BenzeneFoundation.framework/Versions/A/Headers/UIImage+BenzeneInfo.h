//
//  UIImage+BenzeneInfo.h
//  BenzeneFoundation
//
//  Created by sodas on 7/12/14.
//  Copyright (c) 2014 Benzene. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (BenzeneInfo)

- (UIColor *)colorAtPoint:(CGPoint)point;
- (BOOL)getRed:(CGFloat *)r green:(CGFloat *)g blue:(CGFloat *)b alpha:(CGFloat *)a atPoint:(CGPoint)point;

@end
