//
//  NSFileManager+BFPaths.h
//  BenzeneFoundation
//
//  Created by sodas on 11/26/13.
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSFileManager (BFPaths)

- (NSString *)pathOfDocumentFolder;

- (NSString *)pathOfLibraryFolder;
- (NSString *)pathOfLocalLibraryFolder;

- (NSString *)pathOfCacheFolder;
- (NSString *)pathOfLocalCacheFolder;

- (NSString *)pathOfApplicationSupportFolder;
- (NSString *)pathOfLocalApplicationSupportFolder;

- (NSString *)pathOfUniqueTemporaryFolder;
- (NSString *)pathOfUniqueTemporaryFolderWithPrefix:(NSString *)prefix;

- (BOOL)executeBlockWithinTemporaryDirectory:(void(^)(NSString *temporaryDirectoryPath))block;
- (BOOL)executeBlockWithinTemporaryDirectory:(NSError * __autoreleasing *)error
                                       block:(void(^)(NSString *temporaryDirectoryPath))block;
- (BOOL)executeBlockWithinTemporaryDirectory:(NSError * __autoreleasing *)error
                            directoryCleaned:(out BOOL *)outDirCleaned
                                       block:(void(^)(NSString *temporaryDirectoryPath))block;

@end
