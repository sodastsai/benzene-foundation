//
//  UIImage+BenzeneDIP.h
//  BenzeneFoundation
//
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BenzeneFoundation/BFDefines.h>

@interface UIImage (BenzeneDIP)

/**
 * Color the receiver with specified image
 *
 * @param color     Color that should be colored to receiver
 * @return          a copy of receiver that colored with specified color
 */
- (UIImage *)imageColoredWithColor:(UIColor *)color;

/**
 * Color the receiver with specified image using specified blend mode
 *
 * @param color         Color that should be colored to receiver
 * @param blendMode     Blend mode to use
 * @return              a copy of receiver that colored with specified color
 */
- (UIImage *)imageColoredWithColor:(UIColor *)color blendMode:(CGBlendMode)blendMode;

- (UIImage *)resizedImageToFillSize:(CGSize)newSize;

- (UIImage *)proportionallyResizedImageToFillSize:(CGSize)newSize;
- (UIImage *)proportionallyResizedImageToFillSize:(CGSize)newSize cropped:(BOOL)cropped;
- (UIImage *)proportionallyResizedImageToFitSize:(CGSize)targetSize;

- (UIImage *)proportionallyResizedImageToWidth:(CGFloat)width;
- (UIImage *)proportionallyResizedImageToHeight:(CGFloat)height;

- (UIImage *)croppedImageToRect:(CGRect)rect;

- (UIImage *)rotatedImageByRadians:(CGFloat)radians;

@end
