//
//  BFFileMonitor.h
//  BenzeneFoundation
//
//  Created by sodas on 4/15/14.
//  Copyright (c) 2014 Benzene. All rights reserved.
//

#import <Foundation/Foundation.h>

// Ref: http://www.cocoanetics.com/2013/08/monitoring-a-folder-with-gcd/

@class BFFileMonitor;

typedef void (^BFFileMonitorHandler)(BFFileMonitor *);

@interface BFFileMonitor : NSObject

@property (nonatomic, strong, readonly) NSString *path;
@property (nonatomic, assign, getter=isStarted, readonly) BOOL started;

@property (nonatomic, assign) dispatch_queue_t dispatchQueue;
@property (nonatomic, copy) BFFileMonitorHandler startMonitorHandler;
@property (nonatomic, copy) BFFileMonitorHandler stopMonitorHandler;

+ (instancetype)fileMonitorWithPath:(NSString *)path block:(BFFileMonitorHandler)block;
- (instancetype)initWithPath:(NSString *)path block:(BFFileMonitorHandler)block;

- (void)startMonitoring;
- (void)stopMonitoring;

- (void)resumeMonitoring;
- (void)pauseMonitoring;

@end
