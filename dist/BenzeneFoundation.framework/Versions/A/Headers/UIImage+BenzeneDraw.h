//
//  UIImage+BenzeneDraw.h
//  BenzeneFoundation
//
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (BenzeneDraw)

/**
 * Get a image which is filled by the color
 *
 * @param color     The color you want to fill in the result image
 * @param size      The size of result image
 * @return          The result image
 */
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;

/**
 * Get an image with a rectangle filled with fill color and stroked with stroke color
 * The size of image and border width are also as the specified value
 *
 * @param fillColor       the fill color of this image
 * @param strokeColor     the stroke color of this image
 * @param size            the size of this imagge
 * @param borderWidth     the width of stroke
 * @return                an image of rectangle with above specified properties
 * @discuss     the value of size should include the border
 *              For example, a border width 2pt and a size with both 10pts in width and height
 *              gives you a 10*10 image which has 2pt stroke and 6*6 fill area
 */
+ (UIImage *)imageWithRectFilledWithColor:(UIColor *)fillColor strokeColor:(UIColor *)strokeColor
                                     size:(CGSize)size borderWidth:(CGFloat)borderWidth;
/**
 * Get an image with a rounded rectangle filled with fill color and stroked with stroke color
 * The size of image and border width are also as the specified value
 *
 * @param fillColor       the fill color of this image
 * @param strokeColor     the stroke color of this image
 * @param size            the size of this imagge
 * @param borderWidth     the width of stroke
 * @param radius          the radius of this image
 * @return                an image of rectangle with above specified properties
 * @discuss     the value of size should include the border
 *              For example, a border width 2pt and a size with both 10pts in width and height
 *              gives you a 10*10 image which has 2pt stroke and 6*6 fill area
 */
+ (UIImage *)imageWithRoundedRectFilledWithColor:(UIColor *)fillColor strokeColor:(UIColor *)strokeColor
                                            size:(CGSize)size borderWidth:(CGFloat)borderWidth radius:(CGFloat)radius;

/**
 * Get an image with a rounded rectangle with an arrow filled with fill color and stroked with stroke color
 * The size of image and border width are also as the specified value
 *
 * @param fillColor       the fill color of this image
 * @param strokeColor     the stroke color of this image
 * @param size            the size of this imagge
 * @param borderWidth     the width of stroke
 * @param radius          the radius of this image
 * @param arrowSize       the width of left arrow
 * @return                an image of rectangle with above specified properties
 * @discuss     the value of size should include the border
 *              For example, a border width 2pt and a size with both 10pts in width and height
 *              gives you a 10*10 image which has 2pt stroke and 6*6 fill area
 */
+ (UIImage *)imageWithBackButtonBackgroundFilledWithColor:(UIColor *)fillColor strokeColor:(UIColor *)strokeColor
                                                     size:(CGSize)size borderWidth:(CGFloat)borderWidth
                                                   radius:(CGFloat)radius arrowSize:(CGFloat)arrowSize;

@end
