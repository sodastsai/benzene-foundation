//
//  UITableViewCell+LazyLoading.h
//  BenzeneFoundation
//
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <UIKit/UIKit.h>

/// Help table view cell to perform any asynchronous job
@interface UITableViewCell (BFLazyLoading)

/**
 * Perform Async Job and call the completion block if the cell is still in use.
 *
 * @param block          Block containing async job.
 *                       This block will be executed in other thread. (By an NSOperationQueue)
 *                       It should return a result object which will be passed in to the completion block
 * @param completion     Completion block to be called when the async job is finished
 *                       The "result" parameter is the return value of "block" which contains the async job.
 *                       It will be called only when the cell is still in use.
 *                       When the cell is assigned with another async job, this completion block will be flushed.
 */
- (void)performAsynchronousJob:(id(^)(void))block completion:(void(^)(id result))completion;

/// Cancel previous set async job
- (void)cancelAsynchronousJob;

@end
