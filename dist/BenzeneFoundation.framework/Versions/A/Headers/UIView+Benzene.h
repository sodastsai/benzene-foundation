//
//  UIView+Benzene.h
//  BenzeneFoundation
//
//  Created by sodas on 2/20/14.
//  Copyright (c) 2014 Benzene. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Benzene)

@property (nonatomic, assign) CGPoint frameOrigin;
@property (nonatomic, assign) CGSize frameSize;

@end

static inline CGPoint CGPointMinusPoint(CGPoint point1, CGPoint point2) {
    return (CGPoint){ .x = point1.x - point2.x, .y = point1.y - point2.y };
}

static inline CGPoint CGPointAddPoint(CGPoint point1, CGPoint point2) {
    return (CGPoint){ .x = point1.x + point2.x, .y = point1.y + point2.y };
}

static inline CGPoint CGPointMultiplyScale(CGPoint point, CGFloat scale) {
    return (CGPoint){ .x = point.x * scale, .y = point.y * scale };
}

extern const CGPoint CGPointNull;

static inline CGSize CGSizeMulitplyScale(CGSize size, CGFloat scale) {
    return (CGSize){ .width = size.width * scale, .height = size.height * scale };
}

static inline CGFloat CGSizeFitScale(CGSize containerSize, CGSize contentSize) {
    if (containerSize.width < containerSize.height) {
        // Use width to find scale
        return containerSize.width / contentSize.width;
    } else {
        // Use height to find scale
        return containerSize.height / contentSize.height;
    }
}
