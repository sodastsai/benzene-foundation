//
//  UIColor+Benzene.h
//  BenzeneFoundation
//
//  Copyright (c) 2013 Benzene. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BenzeneFoundation/BFDefines.h>

@interface UIColor (Benzene)

- (BOOL)isEqualToColor:(UIColor *)color;

/**
 * Creates and returns a color object using value repsented by the RGBA value
 * RGB (0-255), A (0-1)
 *
 * @param R     Red component (0-255)
 * @param G     Green component value (0-255)
 * @param B     Blue component value (0-255)
 * @param A     Alpha component value (0-1)
 * @return      A color represented by this RGBA set
 */
+ (instancetype)colorWithR:(CGFloat)R G:(CGFloat)G B:(CGFloat)B A:(CGFloat)A;

/**
 * Creates and returns a color object using value repsented by the Hex String
 * For example, @"#FF0000" gives you a red UIColor. (R=255, G=0, B=0)
 *
 * @param hexString     The Hex repesentation of this color.
 *                      Both @"#FF0000" and @"FF0000" are acceptable.
 * @return              The color object.
 *                      The color information represented by this object is in the device RGB colorspace.
 */
+ (instancetype)colorWithHexString:(NSString *)hexString;

/**
 * Creates and returns a color object using value repsented by the Hex String and specified alpha value
 * For example, @"#FF0000" gives you a red UIColor. (R=255, G=0, B=0)
 *
 * @param hexString     The Hex repesentation of this color.
 *                      Both @"#FF0000" and @"FF0000" are acceptable.
 * @param alpha         The alpha value of this color.
 * @return              The color object.
 *                      The color information represented by this object is in the device RGB colorspace.
 */
+ (instancetype)colorWithHexString:(NSString *)hexString alpha:(CGFloat)alpha;

/**
 * Creates and returns a color object using value repsented by the Hex value
 * For example, "0xFF0000" gives you a red UIColor. (R=255, G=0, B=0)
 *
 * @param hexValue      The Hex repesentation of this color.
 * @return              The color object.
 *                      The color information represented by this object is in the device RGB colorspace.
 */
+ (instancetype)colorWithHexValue:(NSUInteger)rgbValue BF_DEPRECATED;
+ (instancetype)colorWithRGBHexValue:(NSUInteger)rgbValue;
+ (instancetype)colorWithRGBAHexValue:(NSUInteger)rgbaValue;

/**
 * Creates and returns a color object using value repsented by the Hex value and specified alpha value
 * For example, "0xFF0000" gives you a red UIColor. (R=255, G=0, B=0)
 *
 * @param hexValue      The Hex repesentation of this color.
 * @param alpha         The alpha value of this color.
 * @return              The color object.
 *                      The color information represented by this object is in the device RGB colorspace.
 */
+ (instancetype)colorWithHexValue:(NSUInteger)rgbValue alpha:(CGFloat)alpha;

- (instancetype)saturatedColor:(CGFloat)saturate;
- (instancetype)desaturatedColor:(CGFloat)desaturate;
- (instancetype)grayscaledColor;

- (instancetype)lightenColor:(CGFloat)lighten;
- (instancetype)darkenColor:(CGFloat)darken;

- (instancetype)mixedColorWithColor:(UIColor *)anotherColor;
- (instancetype)mixedColorWithColor:(UIColor *)anotherColor weight:(CGFloat)weight;

- (instancetype)multipliedColorWithColor:(UIColor *)anotherColor;

// http://en.wikipedia.org/wiki/Luma_%28video%29
- (CGFloat)luminance;
- (instancetype)contrastColorInLightColor:(UIColor *)lightColor andDarkColor:(UIColor *)darkColor;
- (instancetype)contrastColorInLightColor:(UIColor *)lightColor
                             andDarkColor:(UIColor *)darkColor
                                threshold:(CGFloat)threshold;

- (NSString *)hexValueString;

@end
