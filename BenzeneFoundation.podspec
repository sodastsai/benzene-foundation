Pod::Spec.new do |s|

  # -- Package Information  ------------------------------------------------------------------------------------------ #

  s.name        = "BenzeneFoundation"
  s.version     = "0.5.1"

  s.summary     = "Extension of UIKit and Foundation"
  s.description = "Collections of extensions/utilities from Wantoto Inc. for UIKit and Foundation"
  s.homepage    = "https://bitbucket.org/wantoto/wantotokit"

  s.license     = "BSD"
  s.author      = {
    "sodastsai" => "sodas@wantoto.com",
  }

  # -- Base source and conf ------------------------------------------------------------------------------------------ #

  s.source       = {
    :git => "https://bitbucket.org/wantoto/wantotokit.git",
    :tag => "v#{s.version.to_s}"
  }
  s.platform     = :ios, "7.0"
  s.requires_arc = true
  s.xcconfig     = { "OTHER_LDFLAGS" => "-ObjC" }

  # -- Subspecs ------------------------------------------------------------------------------------------------------ #

  s.default_subspecs = "Foundation"

  s.subspec "Foundation" do |ss|
    ss.source_files         = "BenzeneFoundation/BenzeneFoundation/**/*.{h,m}"
    ss.public_header_files  = "BenzeneFoundation/BenzeneFoundation/**/*.h"
    ss.private_header_files = "BenzeneFoundation/BenzeneFoundation/**/*_Internal.h"
    ss.resource_bundles     = {
      "BenzeneFoundation" => [
        "BenzeneFoundation/BenzeneFoundation/**/*.{png,pdf,lproj,json,xib,storyboard}"
      ]
    }
    ss.frameworks           = "Foundation", "CoreGraphics"
    ss.dependency "libextobjc", '~> 0.4'
  end

  s.subspec "UIKit" do |ss|
    ss.ios.source_files         = "BenzeneFoundation/BenzeneUIKit/**/*.{h,m}"
    ss.ios.public_header_files  = "BenzeneFoundation/BenzeneUIKit/**/*.h"
    ss.ios.private_header_files = "BenzeneFoundation/BenzeneUIKit/**/*_Internal.h"
    ss.ios.resource_bundles     = {
      "BenzeneUIKit" => [
        "BenzeneFoundation/BenzeneUIKit/**/*.{png,pdf,lproj,json,xib,storyboard}"
      ]
    }
    ss.ios.frameworks           = "Foundation", "CoreGraphics", "UIKit"
    ss.ios.dependency "BenzeneFoundation/Foundation"
    ss.ios.dependency "libextobjc", '~> 0.4'
  end

end
