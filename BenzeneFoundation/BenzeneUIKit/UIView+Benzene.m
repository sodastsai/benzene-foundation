//
//  UIView+Benzene.m
//  BenzeneFoundation
//
//  BSD License
//
//  Copyright (c) 2012-2015, Wantoto Inc.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import "UIView+Benzene.h"
#import "BFKeyedSubscription.h"

const CGPoint CGPointNull = (CGPoint){ .x = INFINITY, .y = INFINITY};

@implementation UIView (Benzene)

- (CGPoint)frameOrigin {
    return self.frame.origin;
}

- (void)setFrameOrigin:(CGPoint)origin {
    [self setFrame:(CGRect){origin, self.frame.size}];
}

- (CGSize)frameSize {
    return self.frame.size;
}

- (void)setFrameSize:(CGSize)size {
    [self setFrame:(CGRect){self.frame.origin, size}];
}

#pragma mark - Auto Layout Helpers

+ (NSRegularExpression *)_regularExpressionForReplacingVariableInConstraintString {
    static NSRegularExpression *regex;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        regex = [NSRegularExpression regularExpressionWithPattern:@"\\$view(\\d+)"
                                                          options:NSRegularExpressionCaseInsensitive
                                                            error:nil];
    });
    return regex;
}

+ (NSCache *)_constraintStringProcessingCache {
    static NSCache *constraintStringProcessingCache;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{ constraintStringProcessingCache = [[NSCache alloc] init]; });
    return constraintStringProcessingCache;
}

+ (NSString *)_processedConstraintStringFromRawString:(NSString *)rawConstraintString {
    NSCache *cache = [self _constraintStringProcessingCache];
    NSString *result = cache[rawConstraintString];
    if (!result) {
        result = rawConstraintString;
        if ([result rangeOfString:@"$view"].location != NSNotFound) {
            NSRegularExpression *regex = [[self class] _regularExpressionForReplacingVariableInConstraintString];
            result = [regex stringByReplacingMatchesInString:result
                                                     options:0
                                                       range:NSMakeRange(0, [result length])
                                                withTemplate:@"view$1"];
        }
        result = [result stringByReplacingOccurrencesOfString:@"$self" withString:@"self_view"];

        cache[rawConstraintString] = result;
    }
    return result;
}


#pragma mark - Main

- (void)addConstraintsFromStringArray:(NSArray *)constraints {
    [self addConstraintsFromStringArray:constraints views:nil];
}

- (void)addConstraintsFromStringArray:(NSArray *)constraints views:(NSArray *)views {
    [constraints enumerateObjectsUsingBlock:^(NSString *constraintString, NSUInteger idx, BOOL *stop) {
        [self addConstraintFromString:constraintString views:views];
    }];
}

- (void)addConstraintFromString:(NSString *)constraint {
    [self addConstraintFromString:constraint views:nil];
}

- (void)addConstraintFromString:(NSString *)inConstraint views:(NSArray *)views {
    if (inConstraint == nil || inConstraint.length <= 0)
        return;

    // Replace Variable
    NSString *constraint = [[self class] _processedConstraintStringFromRawString:inConstraint];

    // Generate View Dictionary
    NSMutableDictionary *viewDict = [NSMutableDictionary dictionaryWithCapacity:[views count] + 1];
    [views enumerateObjectsUsingBlock:^(UIView *view, NSUInteger idx, BOOL *stop) {
        NSString *key = [NSString stringWithFormat:@"view%lu", (unsigned long)idx];
        viewDict[key] = view;
        [view setTranslatesAutoresizingMaskIntoConstraints:NO];
    }];
    viewDict[@"self_view"] = self;

    // Setup views
    [self setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.superview setNeedsUpdateConstraints];
    [self.superview
     addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:constraint options:0 metrics:nil views:viewDict]];
}

- (void)removeAllRelatedConstraints {
    NSMutableArray *removeConstraints = [NSMutableArray arrayWithCapacity:[[self.superview constraints] count]];

    [[self.superview constraints]
     enumerateObjectsUsingBlock:^(NSLayoutConstraint *constraint, NSUInteger idx, BOOL *stop) {
         if ([constraint.firstItem isEqual:self] || [constraint.secondItem isEqual:self]) {
             [removeConstraints addObject:constraint];
         }
     }];
    [self.superview removeConstraints:removeConstraints];
}


#pragma mark - Convenience

- (void)becomeSuperViewSizeDeterminant {
    [self becomeSuperViewSizeDeterminantInAxis:UILayoutConstraintAxisHorizontal];
    [self becomeSuperViewSizeDeterminantInAxis:UILayoutConstraintAxisVertical];
}

- (void)becomeSuperViewSizeDeterminantInAxis:(UILayoutConstraintAxis)Axis {
    NSLayoutConstraint *constraint1 = nil;
    NSLayoutConstraint *constraint2 = nil;

    if (Axis == UILayoutConstraintAxisVertical) {
        constraint1 = [NSLayoutConstraint constraintWithItem:self.superview
                                                   attribute:NSLayoutAttributeBottom
                                                   relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                      toItem:self
                                                   attribute:NSLayoutAttributeBottom
                                                  multiplier:1
                                                    constant:0];

        constraint2 = [NSLayoutConstraint constraintWithItem:self.superview
                                                   attribute:NSLayoutAttributeTop
                                                   relatedBy:NSLayoutRelationLessThanOrEqual
                                                      toItem:self
                                                   attribute:NSLayoutAttributeTop
                                                  multiplier:1
                                                    constant:0];
    } else {

        constraint1 = [NSLayoutConstraint constraintWithItem:self.superview
                                                   attribute:NSLayoutAttributeRight
                                                   relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                      toItem:self
                                                   attribute:NSLayoutAttributeRight
                                                  multiplier:1
                                                    constant:0];

        constraint2 = [NSLayoutConstraint constraintWithItem:self.superview
                                                   attribute:NSLayoutAttributeLeft
                                                   relatedBy:NSLayoutRelationLessThanOrEqual
                                                      toItem:self
                                                   attribute:NSLayoutAttributeLeft
                                                  multiplier:1
                                                    constant:0];
    }
    // constraint1.priority = UILayoutPriorityRequired;
    // constraint2.priority = UILayoutPriorityRequired;
    [self.superview addConstraint:constraint1];
    [self.superview addConstraint:constraint2];
}

- (void)addConstraintOfCenterAlignToSuperViewInAxis:(UILayoutConstraintAxis)axis {
    NSLayoutAttribute attr = NSLayoutAttributeCenterX;
    if (axis == UILayoutConstraintAxisVertical) {
        attr = NSLayoutAttributeCenterY;
    }

    [self setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.superview addConstraint:[NSLayoutConstraint constraintWithItem:self
                                                               attribute:attr
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.superview
                                                               attribute:attr
                                                              multiplier:1
                                                                constant:0]];
}

- (void)centerAlignToView:(UIView *)view inAxis:(UILayoutConstraintAxis)axis {

    UIView *superView = self.superview;
    NSAssert([view isDescendantOfView:superView], @"Destination view is not under the current view's super view.");

    if ([view isDescendantOfView:superView]) {

        NSLayoutAttribute attr = (axis == UILayoutConstraintAxisHorizontal)
            ? NSLayoutAttributeCenterX : NSLayoutAttributeCenterY;
        NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self attribute:attr
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:view attribute:attr
                                                                     multiplier:1 constant:0];
        [superView addConstraint:constraint];
    }
}

- (NSLayoutConstraint *)constraintOfHavingSameHeightWithView:(UIView *)view adjustment:(CGFloat)adjustment {

    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self
                                                                  attribute:NSLayoutAttributeHeight
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:view
                                                                  attribute:NSLayoutAttributeHeight
                                                                 multiplier:1 constant:adjustment];
    return constraint;
}

- (NSLayoutConstraint *)constraintOfHavingSameWidthWithView:(UIView *)view adjustment:(CGFloat)adjustment {

    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self
                                                                  attribute:NSLayoutAttributeWidth
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:view
                                                                  attribute:NSLayoutAttributeWidth
                                                                 multiplier:1 constant:adjustment];
    return constraint;
}

@end
