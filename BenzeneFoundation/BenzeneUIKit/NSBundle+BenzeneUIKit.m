//
//  NSBundle+BenzeneUIKit.m
//  BenzeneFoundation
//
//  Created by sodas on 2/5/15.
//  Copyright (c) 2015 Wantoto Inc. All rights reserved.
//

#import "NSBundle+BenzeneUIKit.h"

@implementation NSBundle (BenzeneUIKit)

+ (instancetype)benzeneUIKitBundle {
    static NSBundle *sharedBenzeneBundle = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *benzeneBundlePath = [[NSBundle mainBundle] pathForResource:@"BenzeneUIKit" ofType:@"bundle"];
        sharedBenzeneBundle = [NSBundle bundleWithPath:benzeneBundlePath];
    });
    return sharedBenzeneBundle;
}

@end
