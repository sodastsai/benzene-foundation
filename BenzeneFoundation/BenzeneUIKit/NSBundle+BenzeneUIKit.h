//
//  NSBundle+BenzeneUIKit.h
//  BenzeneFoundation
//
//  Created by sodas on 2/5/15.
//  Copyright (c) 2015 Wantoto Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (BenzeneUIKit)

+ (instancetype)benzeneUIKitBundle;

@end
