//
//  BFDebugPanel.h
//  BenzeneFoundation
//
//  BSD License
//
//  Copyright (c) 2012-2015, Wantoto Inc.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import <UIKit/UIKit.h>
#import <BenzeneFoundation/BFDefines.h>

#if DEBUG

@protocol BFDebugPanelViewDelegate;

@interface BFDebugPanelView : UIView

+ (instancetype)sharedPanel;

- (void)displayAtPosition:(CGPoint)position;
- (void)displayAtRect:(CGRect)rect;

@property (nonatomic, weak) id<BFDebugPanelViewDelegate> delegate;

@end

@protocol BFDebugPanelViewDelegate <NSObject>

@optional

/**
 *  Callback to be called when the close button on the toolbar of debug panel clicked.
 *  If delegate implements this method, it's delegate's responsiblilty to remove this
 *  debug panel view from its super view
 *
 *  @param debugPanelView the debug panel view being closed.
 */
- (void)debugPanelViewDidClose:(BFDebugPanelView *)debugPanelView;

@end

// Override BFLogs

#ifdef BFLog
#undef BFLog
#endif
#define BFLog(args...) BFDebugLogWithPanel(__FILE__, __LINE__, __PRETTY_FUNCTION__, args)

#ifdef BFSimpleLog
#undef BFSimpleLog
#endif
#define BFSimpleLog(args...) BFSimpleDebugLogWithPanel(args)

BF_EXTERN
void BFDebugLogWithPanel(const char *file, int line, const char *func, NSString *format, ...) NS_FORMAT_FUNCTION(4,5);

BF_EXTERN void BFSimpleDebugLogWithPanel(NSString *format, ...) NS_FORMAT_FUNCTION(1,2);

#endif
