//
//  NSData+Benzene.m
//  BenzeneFoundation
//
//  BSD License
//
//  Copyright (c) 2012-2015, Wantoto Inc.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import "NSData+Benzene.h"
#import "BFDefines_Internal.h"
#import "BFError.h"
#import "BFDefines.h"
#import "BFFunctionUtilities.h"

BFLoadPartialBytesResultCode BFLoadPartialBytes(void *output, const char *path, size_t location, size_t length) {
    if (!output) {
        return BFLoadPartialBytesNoOutputBuffer;
    }

    FILE *file = fopen(path, "rb");
    if (!file) {
        return BFLoadPartialBytesCannotOpenFile;
    }
    
    if (fseek(file, location, SEEK_SET) != 0) {
        fclose(file);
        return BFLoadPartialBytesCannotSetFilePosition;
    }

    size_t chunk = 1;
    if (fread(output, chunk, length, file) != chunk * length) {
        fclose(file);
        memset(output, '\0', length);
        return BFLoadPartialBytesCannotReadFile;
    }
    
    fclose(file);
    return BFLoadPartialBytesSuccess;
}

@implementation NSData (Benzene)

+ (NSData *)randomDataOfLength:(size_t)length {
    // Ref: http://robnapier.net/aes-commoncrypto
    NSMutableData *data = [[NSMutableData alloc] initWithLength:length];
    int result = SecRandomCopyBytes(kSecRandomDefault, length, data.mutableBytes);
    return (result==0) ? [NSData dataWithData:data] : nil;
}

+ (instancetype)dataWithContentsOfFile:(NSString *)path range:(NSRange)range
                                 error:(out NSError *__autoreleasing *)error {
    // Create output buffer
    NSMutableData *data = [NSMutableData dataWithLength:range.length];
    
    // Go
    int result = BFLoadPartialBytes([data mutableBytes], [path UTF8String], range.location, range.length);
    if (result == BFLoadPartialBytesSuccess) {
        return [NSData dataWithData:data];
    } else {
        NSString *msg;
        if (result == BFLoadPartialBytesCannotOpenFile) {
            msg = @"Cannot open file for reading";
        } else if (result == BFLoadPartialBytesCannotSetFilePosition) {
            msg = @"Cannot set file position indicator";
        } else if (result == BFLoadPartialBytesCannotReadFile) {
            msg = @"Failed to read file";
        }
        
        if (msg) {
            BFOutputError(error, BFBenznenFoundationIOErrorDomain,
                          BFBenzeneFoundationLowLevelIOError, msg);
        }
        return nil;
    }
}

- (instancetype)subdataFromIndex:(NSUInteger)index {
    return [self subdataWithRange:NSMakeRange(index, [self length]-index)];
}

- (instancetype)subdataToIndex:(NSUInteger)index {
    return [self subdataWithRange:NSMakeRange(0, index)];
}

@end
