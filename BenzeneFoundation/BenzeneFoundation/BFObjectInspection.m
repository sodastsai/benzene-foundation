//
//  BFObjectInspection.m
//  BenzeneFoundation
//
//  BSD License
//
//  Copyright (c) 2012-2015, Wantoto Inc.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import "BFObjectInspection.h"
#import "BFFunctionUtilities.h"
#import <objc/runtime.h>
#import <libextobjc/extobjc.h>

static char BFObjectInspectionPropertyKeySetAssociationKey;
static char BFObjectInspectionReadwriteOnlyPropertyKeySetAssociationKey;

@implementation BFObjectInspection

+ (NSSet *)propertyKeySetOfObject:(id)object {
    return [self propertyKeySetOfClass:[object class] onlyReadwriteProperties:NO];
}

+ (NSSet *)propertyKeySetOfClass:(Class)ObjectClass {
    return [self propertyKeySetOfClass:ObjectClass onlyReadwriteProperties:NO];
}

+ (NSSet *)propertyKeySetOfObject:(id)object onlyReadwriteProperties:(BOOL)onlyReadwrite {
    return [self propertyKeySetOfClass:[object class] onlyReadwriteProperties:onlyReadwrite];
}

+ (NSSet *)propertyKeySetOfClass:(Class)ObjectClass onlyReadwriteProperties:(BOOL)onlyReadwrite {
    char associationKey = onlyReadwrite ?
        BFObjectInspectionReadwriteOnlyPropertyKeySetAssociationKey : BFObjectInspectionPropertyKeySetAssociationKey;
    NSSet *result = objc_getAssociatedObject(ObjectClass, &associationKey);
    if (!result) {
        @autoreleasepool {
            NSMutableSet *_result = [NSMutableSet set];

            Class CurrentClass = ObjectClass;
            while (CurrentClass) {
                unsigned int propertyArrayCount = 0;
                objc_property_t *__block propertyArray = class_copyPropertyList(CurrentClass, &propertyArrayCount);
                @onExit {
                    BFFreeCMemoryBlock(propertyArray);
                };

                for (NSUInteger i=0; i<propertyArrayCount; ++i) {
                    objc_property_t property = propertyArray[i];
                    ext_propertyAttributes *__block attributes = ext_copyPropertyAttributes(property);
                    @onExit {
                        BFFreeCMemoryBlock(attributes);
                    };

                    if (onlyReadwrite && attributes->readonly && !attributes->ivar) {
                        continue;
                    }

                    NSString *key = @(property_getName(property));
                    [_result addObject:key];
                }

                // Go to super class
                CurrentClass = [CurrentClass superclass];
            }

            // Keep result
            result = [NSSet setWithSet:_result];
            if (result) {
                objc_setAssociatedObject(ObjectClass,
                                         &associationKey,
                                         result,
                                         OBJC_ASSOCIATION_RETAIN_NONATOMIC);
            }
        }
    }
    return result;
}

@end
