//
//  NSArray+Benzene.m
//  BenzeneFoundation
//
//  BSD License
//
//  Copyright (c) 2012-2015, Wantoto Inc.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import "NSArray+Benzene.h"

@implementation NSArray (Benzene)

- (instancetype)arrayByReversingArray {
    return [[self reverseObjectEnumerator] allObjects];
}

- (instancetype)subarrayFromIndex:(NSUInteger)from {
    if (from==[self count]) {
        return [[self class] array];
    } else if (from < [self count]) {
        return [self subarrayWithRange:NSMakeRange(from, [self count] - from)];
    } else {
        return nil;
    }
}

- (instancetype)subarrayToIndex:(NSUInteger)to {
    if (to==0) {
        return [[self class] array];
    } else if (to < [self count]) {
        return [self subarrayWithRange:NSMakeRange(0, to)];
    } else {
        return nil;
    }
}

- (instancetype)filteredArrayByKeypath:(NSString *)keyPath {
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:[self count]];
    [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj valueForKeyPath:keyPath]) {
            [result addObject:obj];
        }
    }];
    return [[self class] arrayWithArray:result];
}

- (instancetype)filteredArrayByBooleanKeypath:(NSString *)keyPath {
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:[self count]];
    [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        id target = [obj valueForKeyPath:keyPath];
        if ([target respondsToSelector:@selector(boolValue)] && [target boolValue]) {
            [result addObject:obj];
        }
    }];
    return [[self class] arrayWithArray:result];
}

- (instancetype)filteredArrayByKeypath:(NSString *)keyPath withValue:(id)value {
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:[self count]];
    [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([[obj valueForKeyPath:keyPath] isEqual:value]) {
            [result addObject:obj];
        }
    }];
    return [[self class] arrayWithArray:result];
}

- (id)objectAtIndexPath:(NSIndexPath *)indexPath {
    id result = self;
    for (NSUInteger indexPathPosition = 0; indexPathPosition < [indexPath length]; ++indexPathPosition) {
        if ([result respondsToSelector:@selector(objectAtIndex:)]) {
            result = [result objectAtIndex:[indexPath indexAtPosition:indexPathPosition]];
        } else {
            return nil;
        }
    }
    return result;
}

+ (void)enumerateObjectsInArrays:(NSArray *)arrayList usingBlock:(void (^)(id, NSUInteger, BOOL *))block {
    [arrayList enumerateObjectsUsingBlock:^(NSArray *array, NSUInteger idx, BOOL *stop) {
        [array enumerateObjectsUsingBlock:block];
    }];
}

- (NSInteger)indexOfLastSameObjectFromHeadWithArray:(NSArray *)array {
    NSUInteger lastSameObjectIndexFromHead = 0;
    while (lastSameObjectIndexFromHead < MIN([self count], [array count])) {
        if ([self[lastSameObjectIndexFromHead] isEqual:array[lastSameObjectIndexFromHead]]) {
            lastSameObjectIndexFromHead++;
        } else {
            break;
        }
    }
    return lastSameObjectIndexFromHead - (NSInteger)1;
}

@end

@implementation NSMutableArray (Benzene)

- (instancetype)arrayByReversingArray {
    NSMutableArray *mutableArray = [NSMutableArray arrayWithArray:self];
    [mutableArray reverseArray];
    return mutableArray;
}

- (void)reverseArray {
    NSInteger headIndex = 0;
    NSInteger tailIndex = [self count] - 1;
    while (headIndex < tailIndex) {
        [self exchangeObjectAtIndex:headIndex++ withObjectAtIndex:tailIndex--];
    }
}
    
@end