//
//  BFLog_Internal.h
//  BenzeneFoundation
//
//  Created by sodas on 2/11/15.
//  Copyright (c) 2015 Wantoto Inc. All rights reserved.
//

#import "BFLog.h"

BF_EXTERN NSString *_BFDebugLog(const char *file, int lineNumber, const char *funcName, NSString *body);
BF_EXTERN NSString *_BFSimpleDebugLog(NSString *body);
