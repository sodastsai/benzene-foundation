//
//  BFFileMonitor.m
//  BenzeneFoundation
//
//  BSD License
//
//  Copyright (c) 2012-2015, Wantoto Inc.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import "BFFileMonitor.h"
#import "BFDefines.h"
#import "BFFunctionUtilities.h"
#import <libextobjc/extobjc.h>
#import <pthread.h>

NSString *const BFFileMonitorErrorDomain = @"com.benzene.file-monitor";

@interface BFFileMonitor () {
    int fileDescriptor;
    dispatch_source_t dispatchSource;

    pthread_mutex_t _stateMutex;
}

@property(nonatomic, copy, readwrite) BFFileMonitorHandler handler;
@property(nonatomic, strong) NSString *path;
@property(nonatomic, strong, readonly) dispatch_queue_t workingDispatchQueue;
@property (nonatomic, assign, getter=isSuspended, readwrite) BOOL suspended;

@end

@implementation BFFileMonitor

+ (instancetype)fileMonitorWithPath:(NSString *)path block:(BFFileMonitorHandler)block {
    return [[self alloc] initWithPath:path block:block];
}

+ (pthread_mutexattr_t)mutexAttribute {
    static pthread_mutexattr_t attr;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        pthread_mutexattr_init(&attr);
        pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
    });

    return attr;
}

- (instancetype)initWithPath:(NSString *)path block:(BFFileMonitorHandler)block {
    NSParameterAssert(block!=nil);

    if (self = [super init]) {
        pthread_mutexattr_t attr = [[self class] mutexAttribute];
        pthread_mutex_init(&_stateMutex, &attr);

        _path = path;
        _handler = block;
        _suspended = NO;
        _mask = DISPATCH_VNODE_WRITE;
    }
    return self;
}

- (void)dealloc {
    [self stopMonitoring];

    if (fileDescriptor) {
        close(fileDescriptor), fileDescriptor = 0;
    }

    pthread_mutex_destroy(&_stateMutex);
}

#pragma mark - Method

- (dispatch_queue_t)workingDispatchQueue {
    return self.dispatchQueue ?: dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
}

- (BOOL)isStarted {
    return dispatchSource != nil;
}

- (void)startMonitoring {
    if (self.started) {
        return;
    }
    pthread_mutex_lock(&_stateMutex);
    @onExit {
        pthread_mutex_unlock(&_stateMutex);
    };
    if (self.started) {
        return;
    }

    dispatch_sync(self.workingDispatchQueue, ^{
        typeof(self) __weak wSelf = self;

        // Create source
        fileDescriptor = open([_path fileSystemRepresentation], O_EVTONLY);
        if (!fileDescriptor) {
            return;
        }
        dispatchSource = dispatch_source_create(DISPATCH_SOURCE_TYPE_VNODE,
                                                fileDescriptor,
                                                self.mask,
                                                self.workingDispatchQueue);

        dispatch_source_set_registration_handler(dispatchSource, ^{
            typeof(wSelf) __strong sSelf = wSelf;
            BFExecuteBlock(sSelf.startMonitorHandler, sSelf);
        });

        dispatch_source_set_event_handler(dispatchSource, ^{
            typeof(wSelf) __strong sSelf = wSelf;
            BFExecuteBlock(sSelf.handler, sSelf);
        });

        dispatch_source_set_cancel_handler(dispatchSource, ^{
            typeof(wSelf) __strong sSelf = wSelf;
            if (sSelf && sSelf->fileDescriptor) {
                close(sSelf->fileDescriptor), sSelf->fileDescriptor = 0;
            }
            BFExecuteBlock(sSelf.stopMonitorHandler, sSelf);
        });

        // Go
        dispatch_resume(dispatchSource);
    });
}

- (void)stopMonitoring {
    if (!self.started) {
        return;
    }
    pthread_mutex_lock(&_stateMutex);
    @onExit {
        pthread_mutex_unlock(&_stateMutex);
    };
    if (!self.started) {
        return;
    }

    dispatch_sync(self.workingDispatchQueue, ^{
        if (self.suspended) {
            dispatch_resume(dispatchSource);
        }
        
        dispatch_source_cancel(dispatchSource);
        dispatchSource = nil;
    });
}

- (void)resumeMonitoring {
    if (!self.started || !self.suspended) {
        return;
    }
    pthread_mutex_lock(&_stateMutex);
    @onExit {
        pthread_mutex_unlock(&_stateMutex);
    };
    if (self.started && self.suspended) {
        dispatch_sync(self.workingDispatchQueue, ^{
            if ([self isStarted] && self.suspended) {
                self.suspended = NO;
                dispatch_resume(dispatchSource);
                BFExecuteAsyncBlockOnDispatchQueue(self.workingDispatchQueue, self.resumeMonitorHandler, self);
            }
        });
    }
}

- (void)pauseMonitoring {
    if (!self.started || self.suspended) {
        return;
    }
    pthread_mutex_lock(&_stateMutex);
    @onExit {
        pthread_mutex_unlock(&_stateMutex);
    };
    if (self.started && !self.suspended) {
        dispatch_sync(self.workingDispatchQueue, ^{
            if ([self isStarted] && !self.suspended) {
                self.suspended = YES;
                dispatch_suspend(dispatchSource);
                BFExecuteAsyncBlockOnDispatchQueue(self.workingDispatchQueue, self.pauseMonitorHandler, self);
            }
        });
    }
}

@end
