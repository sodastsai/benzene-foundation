//
//  BFDictObject.m
//  BenzeneFoundation
//
//  BSD License
//
//  Copyright (c) 2012-2015, Wantoto Inc.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import "BFDictObject.h"
#import "BFDictionaryObjectHelper.h"

@interface BFDictObject ()

@end

@implementation BFDictObject

- (instancetype)initWithDictionary:(NSMutableDictionary *)dictionary {
    if (self = [super init]) {
        _dictionary = dictionary;
    }
    return self;
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector {
    return [super methodSignatureForSelector:aSelector]?:
        [BFDictionaryObjectHelper methodSignatureForDynamicPropertySelector:aSelector inClass:[self class]];
}

- (void)forwardInvocation:(NSInvocation *)anInvocation {
    [BFDictionaryObjectHelper forwardInvocation:anInvocation dictionary:self.dictionary inClass:[self class]]?:
        [super forwardInvocation:anInvocation];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    [BFDictionaryObjectHelper setValue:value forUndefinedKey:key dictionary:self.dictionary inClass:[self class]]?:
        [super setValue:value forUndefinedKey:key];
}

- (id)valueForUndefinedKey:(NSString *)key {
    return [BFDictionaryObjectHelper valueForUndefinedKey:key dictionary:self.dictionary inClass:[self class]]?:
        [super valueForUndefinedKey:key];
}

+ (NSArray *)arrayWithDictObjectsFromDictionaryArray:(NSArray *)dictArray {
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:[dictArray count]];
    [dictArray enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
        [result addObject:[[self alloc] initWithDictionary:[obj mutableCopy]]];
    }];
    return [NSArray arrayWithArray:result];
}

@end
