//
//  BFQueue.m
//  BenzeneFoundation
//
//  BSD License
//
//  Copyright (c) 2012-2015, Wantoto Inc.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import "BFQueue.h"
#import <pthread.h>
#import <libextobjc/extobjc.h>

@interface BFQueueContentEnumerator : NSEnumerator{
    NSInteger currentObjectIndex;
    NSInteger objectEndIndex;
    NSUInteger capacity;
}

@property(nonatomic, strong) BFQueue *queue;

- (instancetype)initWithQueue:(BFQueue *)queue;

@end

@interface BFQueue () {
    NSArray *_rep;

    pthread_mutex_t _mutex;
}

@property (nonatomic, strong, readwrite) NSMutableArray *content;
@property (nonatomic, assign) NSUInteger tail;
@property (nonatomic, assign) NSUInteger head;

@end

@implementation BFQueue

+ (instancetype)queue {
    return [[self alloc] initWithCapacity:0];
}

+ (instancetype)queueWithCapacity:(NSUInteger)capacity {
    return [[self alloc] initWithCapacity:capacity];
}

+ (pthread_mutexattr_t)mutexAttribute {
    static pthread_mutexattr_t attr;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        pthread_mutexattr_init(&attr);
        pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
    });

    return attr;
}

- (id)initWithCapacity:(NSUInteger)capacity {
    if (self = [super init]) {
        pthread_mutexattr_t attr = [[self class] mutexAttribute];
        pthread_mutex_init(&_mutex, &attr);

        _content = capacity > 0 ? [NSMutableArray arrayWithCapacity:capacity] : [NSMutableArray array];
    }
    return self;
}

- (void)dealloc {
    pthread_mutex_destroy(&_mutex);
}

- (BOOL)isEqual:(id)object {
    return ((self == object) || ([object isKindOfClass:[self class]] && [self isEqualToQueue:object]));
}

- (NSUInteger)hash {
    return [[self array] hash];
}

- (BOOL)isEqualToQueue:(BFQueue *)queue {
    return [[queue array] isEqualToArray:[self array]];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@: TAIL-> %@ <-HEAD",
            [self class], [[self array] componentsJoinedByString:@", "]];
}

#pragma mark - KVO

+ (NSSet *)keyPathsForValuesAffectingArray {
    return [NSSet setWithObjects:@keypath(BFQueue.new, head), @keypath(BFQueue.new, tail), nil];
}

+ (NSSet *)keyPathsForValuesAffectingCount {
    return [NSSet setWithObjects:@keypath(BFQueue.new, head), @keypath(BFQueue.new, tail), nil];
}

+ (NSSet *)keyPathsForValuesAffectingEmpty {
    return [NSSet setWithObject:@keypath(BFQueue.new, count)];
}

#pragma mark - Methods

- (void)enqueueObject:(id)object {
    pthread_mutex_lock(&_mutex);
    @onExit {
        pthread_mutex_unlock(&_mutex);
    };

    if ([self.delegate respondsToSelector:@selector(queue:willEnqueueObject:)]) {
        [self.delegate queue:self willEnqueueObject:object];
    }

    [self.content addObject:object];
    self.head++;
    _rep = nil;

    if ([self.delegate respondsToSelector:@selector(queue:didEnqueueObject:)]) {
        [self.delegate queue:self didEnqueueObject:object];
    }
}

- (id)dequeue {
    pthread_mutex_lock(&_mutex);
    @onExit {
        pthread_mutex_unlock(&_mutex);
    };

    if (![self isEmpty] && self.tail < [self.content count]) {
        id result = self.content[self.tail];
        if ([self.delegate respondsToSelector:@selector(queue:willDequeueObject:)]) {
            [self.delegate queue:self willDequeueObject:result];
        }

        self.content[self.tail++] = [NSNull null];
        _rep = nil;

        if ([self.delegate respondsToSelector:@selector(queue:didDequeueObject:)]) {
            [self.delegate queue:self didDequeueObject:result];
        }

        return result;
    }
    return nil;
}

- (id)peek {
    pthread_mutex_lock(&_mutex);
    @onExit {
        pthread_mutex_unlock(&_mutex);
    };
    return ([self.content count]==0 || self.tail==self.head) ? nil : self.content[self.tail];
}

- (NSUInteger)count {
    return self.head-self.tail;
}

- (BOOL)isEmpty {
    return [self count]==0;
}

- (NSArray *)dequeueAllObjects {
    pthread_mutex_lock(&_mutex);
    @onExit {
        pthread_mutex_unlock(&_mutex);
    };

    NSArray *result = [[self array] copy];
    if ([self.delegate respondsToSelector:@selector(queue:willDequeueObject:)]) {
        [result enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [self.delegate queue:self willDequeueObject:obj];
        }];
    }

    [self.content removeAllObjects];
    _tail = 0; // Only trigger one KVO event
    self.head = 0;

    if ([self.delegate respondsToSelector:@selector(queue:didDequeueObject:)]) {
        [result enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [self.delegate queue:self didDequeueObject:obj];
        }];
    }

    return result;
}

#pragma mark - Interface

- (NSArray *)array {
    if (!_rep) {
        _rep = [self.content subarrayWithRange:NSMakeRange(self.tail, [self count])];
    }
    return _rep;
}

- (void)compact {
    pthread_mutex_lock(&_mutex);
    @onExit {
        pthread_mutex_unlock(&_mutex);
    };

    if (self.tail==0) return;
    self.content = [[self array] mutableCopy];
    self.tail = 0;
    self.head = [self.content count];
}

- (NSEnumerator *)contentEnumerator {
    return [[BFQueueContentEnumerator alloc] initWithQueue:self];
}

@end

#pragma mark - Enumerator

@implementation BFQueueContentEnumerator

- (instancetype)initWithQueue:(BFQueue *)queue {
    if (self = [super init]) {
        _queue = queue;
        currentObjectIndex = queue.tail;
        objectEndIndex = queue.head;
    }
    return self;
}

- (id)nextObject {
    return currentObjectIndex < objectEndIndex ? self.queue.content[currentObjectIndex++] : nil;
}

@end
