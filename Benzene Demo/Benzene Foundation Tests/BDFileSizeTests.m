//
//  BDFileSizeTests.m
//  Benzene Demo
//
//  BSD License
//
//  Copyright (c) 2012-2015, Wantoto Inc.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import <XCTest/XCTest.h>
#import <BenzeneFoundation/NSFileManager+BFSize.h>
#import <BenzeneFoundation/NSData+Benzene.h>

@interface BDFileSizeTests : XCTestCase
@property (nonatomic, strong) NSBundle *bundle;
@property (nonatomic, strong) NSString *rootPath;
@property (nonatomic, strong) NSFileManager *fileManager;
@end

@implementation BDFileSizeTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.bundle = [NSBundle bundleForClass:[self class]];
    self.rootPath = [self.bundle.bundlePath stringByAppendingPathComponent:@"root"];
    self.fileManager = [NSFileManager defaultManager];
    [self.fileManager createDirectoryAtPath:self.rootPath withIntermediateDirectories:NO attributes:nil error:nil];
}

- (void)tearDown {
    [self.fileManager removeItemAtPath:self.rootPath error:nil];
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

#pragma mark - File

- (void)testFile {
    NSString *path = [self.rootPath stringByAppendingPathComponent:@"test"];
    NSData *data = [NSData randomDataOfLength:1024];
    [self.fileManager createFileAtPath:path contents:data attributes:nil];
    
    NSError *error;
    NSNumber *size = [self.fileManager sizeForItemAtPath:path error:&error];
    XCTAssertNotNil(size);
    XCTAssertEqual([size unsignedIntegerValue], 1024, @"got wrong file size");
}

- (void)testFileEmpty {
    NSString *path = [self.rootPath stringByAppendingPathComponent:@"empty"];
    NSData *data = [NSData randomDataOfLength:0];
    [self.fileManager createFileAtPath:path contents:data attributes:nil];
    
    NSError *error;
    NSNumber *size = [self.fileManager sizeForItemAtPath:path error:&error];
    XCTAssertNotNil(size);
    XCTAssertEqual([size unsignedIntegerValue], 0, @"got wrong file size");
}

#pragma mark - Directory

- (void)testDirectoryEmpty {
    NSString *dPath = [self.rootPath stringByAppendingPathComponent:@"directory"];
    [self.fileManager createDirectoryAtPath:dPath withIntermediateDirectories:NO attributes:nil error:nil];
    
    NSError *error;
    NSNumber *size = [self.fileManager sizeForItemAtPath:dPath error:&error];
    XCTAssertNotNil(size);
    XCTAssertEqual([size unsignedIntegerValue], 0, @"got wrong file size");
}

- (void)testDirectory {
    NSString *dPath = [self.rootPath stringByAppendingPathComponent:@"directory"];
    [self.fileManager createDirectoryAtPath:dPath withIntermediateDirectories:NO attributes:nil error:nil];
    
    NSString *path = [dPath stringByAppendingPathComponent:@"file"];
    NSData *data = [NSData randomDataOfLength:1024];
    [self.fileManager createFileAtPath:path contents:data attributes:nil];
    
    NSError *error;
    NSNumber *size = [self.fileManager sizeForItemAtPath:dPath error:&error];
    XCTAssertNotNil(size);
    XCTAssertEqual([size unsignedIntegerValue], 1024, @"got wrong file size");
}

- (void)testDirectory2 {
    NSString *dPath = [self.rootPath stringByAppendingPathComponent:@"directory"];
    [self.fileManager createDirectoryAtPath:dPath withIntermediateDirectories:NO attributes:nil error:nil];
    
    NSString *path1 = [dPath stringByAppendingPathComponent:@"file1"];
    NSData *data1 = [NSData randomDataOfLength:1024];
    [self.fileManager createFileAtPath:path1 contents:data1 attributes:nil];
    
    NSString *path2 = [dPath stringByAppendingPathComponent:@"file2"];
    NSData *data2 = [NSData randomDataOfLength:1024];
    [self.fileManager createFileAtPath:path2 contents:data2 attributes:nil];
    
    NSError *error;
    NSNumber *size = [self.fileManager sizeForItemAtPath:dPath error:&error];
    XCTAssertNotNil(size);
    XCTAssertEqual([size unsignedIntegerValue], 1024+1024, @"got wrong file size");
}

- (void)testDirectoryNested {
    NSString *dPath = [self.rootPath stringByAppendingPathComponent:@"directory"];
    [self.fileManager createDirectoryAtPath:dPath withIntermediateDirectories:NO attributes:nil error:nil];
    
    NSString *path1 = [dPath stringByAppendingPathComponent:@"file1"];
    NSData *data1 = [NSData randomDataOfLength:1024];
    [self.fileManager createFileAtPath:path1 contents:data1 attributes:nil];

    NSString *dPath2 = [dPath stringByAppendingPathComponent:@"inner"];
    [self.fileManager createDirectoryAtPath:dPath2 withIntermediateDirectories:NO attributes:nil error:nil];
    
    NSError *error;
    NSNumber *size = [self.fileManager sizeForItemAtPath:dPath error:&error];
    XCTAssertNotNil(size);
    XCTAssertEqual([size unsignedIntegerValue], 1024, @"got wrong file size");
}

- (void)testDirectoryNested2 {
    NSString *dPath = [self.rootPath stringByAppendingPathComponent:@"directory"];
    [self.fileManager createDirectoryAtPath:dPath withIntermediateDirectories:NO attributes:nil error:nil];
    
    NSString *path1 = [dPath stringByAppendingPathComponent:@"file1"];
    NSData *data1 = [NSData randomDataOfLength:1024];
    [self.fileManager createFileAtPath:path1 contents:data1 attributes:nil];
    
    NSString *dPath2 = [dPath stringByAppendingPathComponent:@"inner"];
    [self.fileManager createDirectoryAtPath:dPath2 withIntermediateDirectories:NO attributes:nil error:nil];
    
    NSString *path2 = [dPath2 stringByAppendingPathComponent:@"file2"];
    NSData *data2 = [NSData randomDataOfLength:1024];
    [self.fileManager createFileAtPath:path2 contents:data2 attributes:nil];
    
    NSError *error;
    NSNumber *size = [self.fileManager sizeForItemAtPath:dPath error:&error];
    XCTAssertNotNil(size);
    XCTAssertEqual([size unsignedIntegerValue], 1024+1024, @"got wrong file size");
}

- (void)testDirectoryComplex {
    NSUInteger totalSize = 0;
    
    for (int i = 0; i < 5; i++) {
        
        NSString *dirPath = self.rootPath;
        for (int j = 0; j < i; j++) {
            dirPath = [dirPath stringByAppendingPathComponent:@"path"];
            [self.fileManager createDirectoryAtPath:dirPath withIntermediateDirectories:NO attributes:nil error:nil];
        }
        for (int j = 0; j < i; j++) {
            NSString *filePath = [dirPath stringByAppendingPathComponent:[NSString stringWithFormat:@"file%d", j]];
            NSUInteger size = j * i * 1024;
            NSData *data = [NSData randomDataOfLength:size];
            [self.fileManager createFileAtPath:filePath contents:data attributes:nil];
            totalSize += size;
        }
    }
    
    NSError *error;
    NSNumber *size = [self.fileManager sizeForItemAtPath:self.rootPath error:&error];
    XCTAssertNotNil(size);
    XCTAssertEqual([size unsignedIntegerValue], totalSize, @"got wrong file size");
}

#pragma mark - Not exists

- (void)testItemNotExists {
    NSString *path = [self.rootPath stringByAppendingPathComponent:@"foo"];
    
    NSError *error;
    NSNumber *size = [self.fileManager sizeForItemAtPath:path error:&error];
    XCTAssertNil(size);
}

@end
