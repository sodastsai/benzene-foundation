//
//  BDFunctionUitilityTests.m
//  Benzene Demo
//
//  BSD License
//
//  Copyright (c) 2012-2015, Wantoto Inc.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import <Foundation/Foundation.h>
#import <XCTest/XCTest.h>
#import <BenzeneFoundation/BFFunctionUtilities.h>

@interface BDFunctionUitilityTests : XCTestCase

@end

@implementation BDFunctionUitilityTests

#pragma mark - Numbers

- (void)testBoundary {
    XCTAssertEqual(BFBoundary(12, 5, 10), 10);
    XCTAssertEqual(BFBoundary(-12, 5, 10), 5);
    XCTAssertEqual(BFBoundary(7, 5, 10), 7);

    XCTAssertEqualObjects(BFBoundaryNumber(@12, @5, @10), @10);
    XCTAssertEqualObjects(BFBoundaryNumber(@-12, @5, @10), @5);
    XCTAssertEqualObjects(BFBoundaryNumber(@7, @5, @10), @7);
}

- (void)testAngleAndDegree {
    CGFloat const accuracy = 0.000000001;

    XCTAssertEqualWithAccuracy(BFDegreeFromAngle(M_PI), 180., accuracy);
    XCTAssertEqualWithAccuracy(BFDegreeFromAngle(M_PI / 2), 90., accuracy);
    XCTAssertEqualWithAccuracy(BFDegreeFromAngle(M_PI * 4 / 3), 240., accuracy);
    XCTAssertEqualWithAccuracy(BFDegreeFromAngle(2 * M_PI * 5 / 3), 600., accuracy);
    XCTAssertEqualWithAccuracy(BFDegreeFromAngle(-M_PI * 4 / 3), -240., accuracy);

    XCTAssertEqualWithAccuracy(BFAngleFromDegree(180.), M_PI, accuracy);
    XCTAssertEqualWithAccuracy(BFAngleFromDegree(90.), M_PI_2, accuracy);
    XCTAssertEqualWithAccuracy(BFAngleFromDegree(-120.), -M_PI * 2 / 3, accuracy);
    XCTAssertEqualWithAccuracy(BFAngleFromDegree(-351.), -1.95 * M_PI, accuracy);

    XCTAssertEqualWithAccuracy(BFNormalizeDegree180(-351.), 9., accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeDegree180(120.), 120., accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeDegree180(73.), 73., accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeDegree180(561.), -159., accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeDegree180(810.5), 90.5, accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeDegree180(-141.), -141., accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeDegree180(-60.), -60., accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeDegree180(-792.1), -72.1, accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeDegree180(0.), 0., accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeDegree180(180.), 180., accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeDegree180(-180.), 180., accuracy);

    XCTAssertEqualWithAccuracy(BFNormalizeDegree360(-351.), 9., accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeDegree360(120.), 120., accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeDegree360(73.), 73., accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeDegree360(561.), 201., accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeDegree360(810.5), 90.5, accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeDegree360(-141.), 219., accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeDegree360(-60.), 300., accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeDegree360(-792.1), 287.9, accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeDegree360(0.), 0., accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeDegree360(180.), 180., accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeDegree360(-180.), 180., accuracy);

    XCTAssertTrue(BFDegreeIsNormalized180(179.));
    XCTAssertTrue(BFDegreeIsNormalized180(64.2));
    XCTAssertTrue(BFDegreeIsNormalized180(-32.));
    XCTAssertTrue(BFDegreeIsNormalized180(-143.));
    XCTAssertFalse(BFDegreeIsNormalized180(-253.3));
    XCTAssertFalse(BFDegreeIsNormalized180(313.));
    XCTAssertTrue(BFDegreeIsNormalized180(180.));
    XCTAssertFalse(BFDegreeIsNormalized180(-180.));

    XCTAssertTrue(BFDegreeIsNormalized360(179.));
    XCTAssertTrue(BFDegreeIsNormalized360(64.2));
    XCTAssertFalse(BFDegreeIsNormalized360(-32.));
    XCTAssertFalse(BFDegreeIsNormalized360(-143.));
    XCTAssertFalse(BFDegreeIsNormalized360(-253.3));
    XCTAssertTrue(BFDegreeIsNormalized360(313.));
    XCTAssertTrue(BFDegreeIsNormalized360(180.));
    XCTAssertFalse(BFDegreeIsNormalized360(-180.));

    XCTAssertTrue(BFDegreeIsNormalized180(BFNormalizeDegree180(12.)));
    XCTAssertTrue(BFDegreeIsNormalized180(BFNormalizeDegree180(101.51)));
    XCTAssertTrue(BFDegreeIsNormalized180(BFNormalizeDegree180(180.)));
    XCTAssertTrue(BFDegreeIsNormalized180(BFNormalizeDegree180(-180.)));
    XCTAssertTrue(BFDegreeIsNormalized180(BFNormalizeDegree180(360.)));
    XCTAssertTrue(BFDegreeIsNormalized180(BFNormalizeDegree180(0.)));
    XCTAssertTrue(BFDegreeIsNormalized180(BFNormalizeDegree180(-120.)));
    XCTAssertTrue(BFDegreeIsNormalized180(BFNormalizeDegree180(-163.)));
    XCTAssertTrue(BFDegreeIsNormalized180(BFNormalizeDegree180(8125.)));
    XCTAssertTrue(BFDegreeIsNormalized180(BFNormalizeDegree180(-649.)));

    XCTAssertTrue(BFDegreeIsNormalized360(BFNormalizeDegree360(12.)));
    XCTAssertTrue(BFDegreeIsNormalized360(BFNormalizeDegree360(101.51)));
    XCTAssertTrue(BFDegreeIsNormalized360(BFNormalizeDegree360(180.)));
    XCTAssertTrue(BFDegreeIsNormalized360(BFNormalizeDegree360(-180.)));
    XCTAssertTrue(BFDegreeIsNormalized360(BFNormalizeDegree360(360.)));
    XCTAssertTrue(BFDegreeIsNormalized360(BFNormalizeDegree360(0.)));
    XCTAssertTrue(BFDegreeIsNormalized360(BFNormalizeDegree360(-120.)));
    XCTAssertTrue(BFDegreeIsNormalized360(BFNormalizeDegree360(-163.)));
    XCTAssertTrue(BFDegreeIsNormalized360(BFNormalizeDegree360(8125.)));
    XCTAssertTrue(BFDegreeIsNormalized360(BFNormalizeDegree360(-649.)));

    XCTAssertEqualWithAccuracy(BFNormalizeAnglePI(M_PI), M_PI, accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeAnglePI(-M_PI), M_PI, accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeAnglePI(0.), 0., accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeAnglePI(1.8849555922), M_PI * 0.6, accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeAnglePI(10.995574288), M_PI * -0.5, accuracy); // 3.5pi --> -0.5pi

    XCTAssertEqualWithAccuracy(BFNormalizeAngle2PI(M_PI), M_PI, accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeAngle2PI(-M_PI), M_PI, accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeAngle2PI(0.), 0., accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeAngle2PI(1.8849555922), M_PI * 0.6, accuracy);
    XCTAssertEqualWithAccuracy(BFNormalizeAngle2PI(10.995574288), M_PI * 1.5, accuracy); // 3.5pi --> 1.5pi

    XCTAssertTrue(BFAngleIsNormalizedPI(M_PI));
    XCTAssertFalse(BFAngleIsNormalizedPI(-M_PI));
    XCTAssertTrue(BFAngleIsNormalizedPI(M_PI_2));
    XCTAssertFalse(BFAngleIsNormalizedPI(M_PI * 2));
    XCTAssertTrue(BFAngleIsNormalizedPI(-M_PI / 3));

    XCTAssertTrue(BFAngleIsNormalized2PI(M_PI));
    XCTAssertFalse(BFAngleIsNormalized2PI(-M_PI));
    XCTAssertTrue(BFAngleIsNormalized2PI(M_PI_2));
    XCTAssertFalse(BFAngleIsNormalized2PI(M_PI * 2));
    XCTAssertFalse(BFAngleIsNormalized2PI(-M_PI / 3));

    XCTAssertTrue(BFAngleIsNormalizedPI(BFNormalizeAnglePI(M_PI)));
    XCTAssertTrue(BFAngleIsNormalizedPI(BFNormalizeAnglePI(-M_PI)));
    XCTAssertTrue(BFAngleIsNormalizedPI(BFNormalizeAnglePI(0)));
    XCTAssertTrue(BFAngleIsNormalizedPI(BFNormalizeAnglePI(M_PI / 2)));
    XCTAssertTrue(BFAngleIsNormalizedPI(BFNormalizeAnglePI(M_PI * 3.6)));

    XCTAssertTrue(BFAngleIsNormalized2PI(BFNormalizeAngle2PI(M_PI)));
    XCTAssertTrue(BFAngleIsNormalized2PI(BFNormalizeAngle2PI(-M_PI)));
    XCTAssertTrue(BFAngleIsNormalized2PI(BFNormalizeAngle2PI(0)));
    XCTAssertTrue(BFAngleIsNormalized2PI(BFNormalizeAngle2PI(M_PI / 2)));
    XCTAssertTrue(BFAngleIsNormalized2PI(BFNormalizeAngle2PI(M_PI * 3.6)));
}

@end
