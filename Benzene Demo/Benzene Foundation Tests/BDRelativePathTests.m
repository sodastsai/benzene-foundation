//
//  BDRelativePathTests.m
//  Benzene Demo
//
//  BSD License
//
//  Copyright (c) 2012-2015, Wantoto Inc.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import <XCTest/XCTest.h>
#import <BenzeneFoundation/NSURL+Benzene.h>

@interface BDRelativePathTests : XCTestCase

@end

@implementation BDRelativePathTests

- (void)testPathWithTrailingSlash {
    NSURL *url = [NSURL URLWithString:@"http://www.apple.com/imac/"];
    XCTAssertEqualObjects(@"/imac/", [url pathWithTrailingSlash]);

    url = [NSURL URLWithString:@"http://www.apple.com/iphone"];
    XCTAssertEqualObjects(@"/iphone", [url pathWithTrailingSlash]);
}

- (void)testRelativePaths {
    NSArray *tests = @[
        @[@"/Users/sodas/Desktop/", @"/Users/sodas/Desktop", @"."],
        @[@"/Users/sodas/Desktop/a.png", @"/Users/sodas/Desktop", @"a.png"],
        @[@"/Users/sodas/Desktop/a.png", @"/Users/sodas", @"Desktop/a.png"],
        @[@"/Users/mike/Desktop/a.png", @"/Users/sodas", @"../mike/Desktop/a.png"],
        @[@"/Users/mike/Desktop/a.png", @"/Users/sodas/Desktop", @"../../mike/Desktop/a.png"],
        @[@"/Users/mike/Desktop/b.png", @"/Users/mike/Desktop/abc", @"../b.png"],
        @[@"/Users/mike/Desktop/b.png", @"/Users/sodas/Desktop/abc/xyz", @"../../../../mike/Desktop/b.png"],
        @[@"/Users/mike/Desktop/", @"/Users/sodas/Desktop/abc", @"../../../mike/Desktop"],
    ];

    [tests enumerateObjectsUsingBlock:^(NSArray *paths, NSUInteger idx, BOOL *stop) {
        NSString *result = [paths[0] stringByRelativePathToDirectory:paths[1]];
        XCTAssertEqualObjects(result, paths[2]);
    }];
}

- (void)testRelativeURLs {
    NSArray *tests = @[
        @[[NSURL URLWithString:@"http://www.google.com/docs"],
          [NSURL URLWithString:@"http://www.apple.com"],
          @NO],

        @[[NSURL URLWithString:@"http://www.apple.com/docs"],
          [NSURL URLWithString:@"http://www.apple.com"],
          @YES, @"docs"],

        @[[NSURL URLWithString:@"http://www.apple.com/docs"],
          [NSURL URLWithString:@"http://www.apple.com/app"],
          @YES, @"docs"],

        @[[NSURL URLWithString:@"http://www.apple.com/docs"],
          [NSURL URLWithString:@"http://www.apple.com/app/"],
          @YES, @"../docs"],

        @[[NSURL URLWithString:@"http://www.apple.com/docs/"],
          [NSURL URLWithString:@"http://www.apple.com/app"],
          @YES, @"docs"],

        @[[NSURL URLWithString:@"http://www.apple.com/docs/"],
          [NSURL URLWithString:@"http://www.apple.com/app/"],
          @YES, @"../docs"],

        @[[NSURL URLWithString:@"http://www.apple.com/docs/abc/de"],
          [NSURL URLWithString:@"http://www.apple.com/docs/xyz/"],
          @YES, @"../abc/de"],

        @[[NSURL URLWithString:@"http://www.apple.com/docs/abc/de"],
          [NSURL URLWithString:@"http://www.apple.com/docs/xyz"],
          @YES, @"abc/de"],
    ];

    [tests enumerateObjectsUsingBlock:^(NSArray *paths, NSUInteger idx, BOOL *stop) {
        NSURL *result = [paths[0] URLByRelativeURLToDirectory:paths[1]];
        XCTAssertEqualObjects([result absoluteString], [paths[0] absoluteString]);
        if ([paths[2] boolValue]) {
            XCTAssertEqualObjects([result relativePath], paths[3]);
            XCTAssertEqualObjects([[result baseURL] absoluteString], [paths[1] absoluteString]);
        }
    }];
}

- (void)testSubpaths {
    NSArray *tests = @[
        @[@"/Users/sodas/Desktop/", @"/Users/sodas/Desktop", @NO],
        @[@"/Users/sodas/Desktop/", @"/Users/sodas/Desktop/abc", @NO],
        @[@"/Users/sodas/Desktop/abc", @"/Users/sodas/Desktop", @YES],
        @[@"/Users/mike/Desktop/abc", @"/Users/sodas/Desktop", @NO],
    ];

    [tests enumerateObjectsUsingBlock:^(NSArray *paths, NSUInteger idx, BOOL *stop) {
        XCTAssertEqual([paths[0] isSubpathOfPath:paths[1]], [paths[2] boolValue]);
    }];
}

- (void)testSubURLs {
    NSArray *tests = @[
        @[[NSURL URLWithString:@"http://www.google.com/docs"], [NSURL URLWithString:@"http://www.apple.com/docs"], @NO],

        @[[NSURL URLWithString:@"http://www.apple.com/docs"], [NSURL URLWithString:@"http://www.apple.com/docs"], @NO],

        @[[NSURL URLWithString:@"http://www.apple.com/docs/a"],
          [NSURL URLWithString:@"http://www.apple.com/docs"],
          @YES],

        @[[NSURL URLWithString:@"http://google/docs/a"],
          [NSURL URLWithString:@"http://www.apple.com/docs"],
          @NO],

        @[[NSURL URLWithString:@"http://www.apple.com:8080/docs/a"],
          [NSURL URLWithString:@"http://www.apple.com/docs"],
          @NO],

        @[[NSURL URLWithString:@"http://www.apple.com/docs"],
          [NSURL URLWithString:@"http://www.apple.com/docs/a"],
          @NO],

        @[[NSURL fileURLWithPath:@"/docs/a"],
          [NSURL URLWithString:@"http://www.apple.com/docs/a"],
          @NO],

        @[[NSURL fileURLWithPath:@"/docs/a"],
          [NSURL fileURLWithPath:@"/docs"],
          @YES],
    ];

    [tests enumerateObjectsUsingBlock:^(NSArray *paths, NSUInteger idx, BOOL *stop) {
        XCTAssertEqual([paths[0] isSubpathOfURL:paths[1]], [paths[2] boolValue]);
    }];
}

@end
