//
//  BFURLEncoding.m
//  BenzeneFoundation
//
//  BSD License
//
//  Copyright (c) 2012-2015, Wantoto Inc.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import <XCTest/XCTest.h>
#import <BenzeneFoundation/BFURLEncoding.h>

NSDictionary *dict;
NSString *urlEncodedStringOfDict;
NSString *originalString;
NSString *urlEncodedString;

@interface BDURLEncodingTests : XCTestCase
@end

@implementation BDURLEncodingTests

+ (void)setUp {
    [super setUp];
    
    originalString = @"a+-=)*/&?abscd*@DS:;'";
    dict = @{
             @"answer": @42,
             @"key": originalString,
             @"smile": @"XD",
             };
    
    urlEncodedString = @"a%2B-%3D%29%2A%2F%26%3Fabscd%2A%40DS%3A%3B%27";
    urlEncodedStringOfDict = @"answer=42&key=a%2B-%3D%29%2A%2F%26%3Fabscd%2A%40DS%3A%3B%27&smile=XD";
}

+ (void)tearDown {
    [super tearDown];
    dict = nil;
    urlEncodedStringOfDict = urlEncodedString = originalString = nil;
}

- (void)testURLEncode {
    XCTAssertEqualObjects([originalString urlEncodedStringUsingEncoding:NSUTF8StringEncoding], urlEncodedString,
                          @"URL Encoding Failed");
    XCTAssertEqualObjects([dict stringWithURLEncodedValue], urlEncodedStringOfDict, @"URL Encoding for Dict Failed");
}

- (void)testURLDecode {
    XCTAssertEqualObjects([urlEncodedString urlDecodedStringUsingEncoding:NSUTF8StringEncoding], originalString,
                          @"URL Decoding Failed");
    XCTAssertEqualObjects([NSDictionary dictionaryWithContentsOfURLEncodedString:urlEncodedStringOfDict],
                          dict, @"URL Decoding for Dict Failed");
}

@end
