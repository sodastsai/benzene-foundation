//
//  BDImageInfoTestCase.m
//  Benzene Demo
//
//  BSD License
//
//  Copyright (c) 2012-2015, Wantoto Inc.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import <BenzeneFoundation/BenzeneFoundation.h>

@interface BDImageInfoTestCase : XCTestCase

@end

@implementation BDImageInfoTestCase

- (void)testTransparentRect {
    NSBundle *testBundle = [NSBundle bundleForClass:[self class]];
    NSData *data = [NSData dataWithContentsOfFile:[testBundle pathForResource:@"image_info_1" ofType:@"png"]];

    UIImage *image2x = [UIImage imageWithData:data scale:2.0];
    XCTAssertFalse([image2x isTransparentInRect:CGRectMake(25, 25, 50, 50)]);
    XCTAssertFalse([image2x isTransparentInRect:CGRectMake(0, 0, 50, 50)]);
    XCTAssertFalse([image2x isTransparentInRect:CGRectMake(0, 50, 50, 50)]);
    XCTAssertFalse([image2x isTransparentInRect:CGRectMake(50, 50, 50, 50)]);
    XCTAssertTrue([image2x isTransparentInRect:CGRectMake(0, 0, 25, 25)]);

    UIImage *image1x = [UIImage imageWithData:data scale:1.0];
    XCTAssertFalse([image1x isTransparentInRect:CGRectMake(50, 50, 100, 100)]);
    XCTAssertFalse([image1x isTransparentInRect:CGRectMake(0, 0, 100, 100)]);
    XCTAssertFalse([image1x isTransparentInRect:CGRectMake(0, 100, 100, 100)]);
    XCTAssertFalse([image1x isTransparentInRect:CGRectMake(100, 100, 100, 100)]);
    XCTAssertTrue([image1x isTransparentInRect:CGRectMake(0, 0, 50, 50)]);
}

- (void)testImageEnumerator {
    NSBundle *testBundle = [NSBundle bundleForClass:[self class]];
    CGImageRef image = [[UIImage imageWithContentsOfFile:[testBundle pathForResource:@"image_info_2" ofType:@"png"]]
                        CGImage];

    NSUInteger __block count = 0;
    BF_CGImageEneumeratePixels(image, ^(const void *_pixelChannels, size_t x, size_t y, bool *stop) {
        count++;

        const unsigned char *pixelChannels = _pixelChannels;
        const unsigned char r = pixelChannels[0];
        const unsigned char g = pixelChannels[1];
        const unsigned char b = pixelChannels[2];
        const unsigned char a = pixelChannels[3];

        if ((x==0 && y==0) || (x==1 && y==2)) {
            XCTAssertEqual(r, 0, @"x=%ld, y=%ld", x, y);
            XCTAssertEqual(g, 0, @"x=%ld, y=%ld", x, y);
            XCTAssertEqual(b, 0, @"x=%ld, y=%ld", x, y);
            XCTAssertEqual(a, 0, @"x=%ld, y=%ld", x, y);
        }
        if (x==0 && y==3) {
            XCTAssertEqual(r, 0, @"x=%ld, y=%ld", x, y);
            XCTAssertEqual(g, 0, @"x=%ld, y=%ld", x, y);
            XCTAssertEqual(b, 0, @"x=%ld, y=%ld", x, y);
            XCTAssertEqual(a, 0xff, @"x=%ld, y=%ld", x, y);
        }
        if (x==2 && y==1) {
            XCTAssertEqual(r, 0, @"x=%ld, y=%ld", x, y);
            XCTAssertEqual(g, 0xff, @"x=%ld, y=%ld", x, y);
            XCTAssertEqual(b, 0xff, @"x=%ld, y=%ld", x, y);
            XCTAssertEqual(a, 0xff, @"x=%ld, y=%ld", x, y);
        }
        if (x==2 && y==3) {
            XCTAssertEqual(r, 0xff, @"x=%ld, y=%ld", x, y);
            XCTAssertEqual(g, 0x99, @"x=%ld, y=%ld", x, y);
            XCTAssertEqual(b, 0, @"x=%ld, y=%ld", x, y);
            XCTAssertEqual(a, 0xff, @"x=%ld, y=%ld", x, y);
        }
        if (x==3 && y==2) {
            XCTAssertEqual(r, 0xff, @"x=%ld, y=%ld", x, y);
            XCTAssertEqual(g, 0x00, @"x=%ld, y=%ld", x, y);
            XCTAssertEqual(b, 0xff, @"x=%ld, y=%ld", x, y);
            XCTAssertEqual(a, 0xff, @"x=%ld, y=%ld", x, y);
        }
    });
    XCTAssertEqual(count, 16);

    count = 0;
    BF_CGImageEneumeratePixelsInRect(image, CGRectMake(2, 1, 3, 2), ^(const void *pc, size_t x, size_t y, bool *stop) {
        count++;

        const unsigned char *pixelChannels = pc;
        const unsigned char r = pixelChannels[0];
        const unsigned char g = pixelChannels[1];
        const unsigned char b = pixelChannels[2];
        const unsigned char a = pixelChannels[3];

        XCTAssertFalse((x < 2 || x > 3 || y < 1 || y > 3), @"x,y=(%ld,%ld) out of bound", x, y);

        if (x==2 && y==3) {
            XCTAssertEqual(r, 0xff, @"x=%ld, y=%ld", x, y);
            XCTAssertEqual(g, 0x99, @"x=%ld, y=%ld", x, y);
            XCTAssertEqual(b, 0, @"x=%ld, y=%ld", x, y);
            XCTAssertEqual(a, 0xff, @"x=%ld, y=%ld", x, y);
        }
        if (x==3 && y==2) {
            XCTAssertEqual(r, 0xff, @"x=%ld, y=%ld", x, y);
            XCTAssertEqual(g, 0x00, @"x=%ld, y=%ld", x, y);
            XCTAssertEqual(b, 0xff, @"x=%ld, y=%ld", x, y);
            XCTAssertEqual(a, 0xff, @"x=%ld, y=%ld", x, y);
        }
    });
    XCTAssertEqual(count, 4);

    count = 0;
    BF_CGImageEneumeratePixelsInRect(image, CGRectMake(1, 1, 3, 2), ^(const void *pc, size_t x, size_t y, bool *stop) {
        count++;
    });
    XCTAssertEqual(count, 6);
}

@end
