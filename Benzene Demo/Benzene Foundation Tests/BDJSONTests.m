//
//  BFJSONTest.m
//  BenzeneFoundation
//
//  BSD License
//
//  Copyright (c) 2012-2015, Wantoto Inc.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import <XCTest/XCTest.h>
#import <BenzeneFoundation/BFJson.h>

NSString *arrayJsonString;
NSData *arrayJsonData;
NSArray *array;

NSString *dictJsonString;
NSData *dictJsonData;
NSDictionary *dict;

@interface BDJSONTests : XCTestCase
@end

@implementation BDJSONTests

+ (void)setUp {
    [super setUp];
    
    array = @[@42, @"XD", @1.2];
    arrayJsonString = @"[42,\"XD\",1.2]";
    arrayJsonData = [arrayJsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    dict = @{@"answer": @42, @"smile": @"XD"};
    dictJsonString = @"{\"answer\":42,\"smile\":\"XD\"}";
    dictJsonData = [dictJsonString dataUsingEncoding:NSUTF8StringEncoding];
}

+ (void)tearDown {
    array = nil;
    dict = nil;
    arrayJsonString = dictJsonString = nil;
    arrayJsonData = dictJsonData = nil;
}

- (void)testArrayAndJSONString {
    XCTAssertEqualObjects([array jsonString], arrayJsonString, @"Failed to convert array to JSON string");
    XCTAssertEqualObjects([arrayJsonString jsonObject], array, @"Failed to convert JSON string to array");
    XCTAssertEqualObjects([NSArray arrayWithContentsOfJsonString:arrayJsonString], array,
                          @"Failed to create array from JSON string");
}

- (void)testArrayAndJSONData {
    XCTAssertEqualObjects([array jsonData], arrayJsonData, @"Failed to convert array to JSON data");
    XCTAssertEqualObjects([arrayJsonData jsonObject], array, @"Failed to convert JSON data to array");
    XCTAssertEqualObjects([NSArray arrayWithContentsOfJsonData:arrayJsonData], array,
                          @"Failed to create array from JSON data");
}


- (void)testDictAndJSONString {
    XCTAssertEqualObjects([dict jsonString], dictJsonString, @"Failed to convert dict to JSON string");
    XCTAssertEqualObjects([dictJsonString jsonObject], dict, @"Failed to convert JSON string to dict");
    XCTAssertEqualObjects([NSDictionary dictionaryWithContentsOfJsonString:dictJsonString], dict,
                          @"Failed to create dict from JSON string");
}

- (void)testDictAndJSONData {
    XCTAssertEqualObjects([dict jsonData], dictJsonData, @"Failed to convert dict to JSON data");
    XCTAssertEqualObjects([dictJsonData jsonObject], dict, @"Failed to convert JSON data to dict");
    XCTAssertEqualObjects([NSDictionary dictionaryWithContentsOfJsonData:dictJsonData], dict,
                          @"Failed to create dict from JSON data");
}

@end
