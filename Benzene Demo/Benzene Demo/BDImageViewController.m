//
//  BDImageViewController.m
//  Benzene Demo
//
//  BSD License
//
//  Copyright (c) 2012-2015, Wantoto Inc.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import "BDImageViewController.h"
#import <BenzeneFoundation/BenzeneFoundation.h>

@interface BDImageViewController ()

@end

@implementation BDImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *image = [UIImage imageNamed:@"wantoto_logo"];
    image = [UIImage imageWithCGImage:image.CGImage scale:[[UIScreen mainScreen] scale]
                          orientation:image.imageOrientation];
    self.originalImageView.image = image;
    NSLog(@"#%@", [[image colorAtPoint:CGPointMake(image.size.width/2, image.size.height/2)] hexValueString]);
    
//    UIImage *processedImage = [image croppedImageToRect:CGRectMake(160.f, 25.f, 300.f, 50.f)];
    UIImage *processedImage = [image rotatedImageByAngle:M_PI/6];
//    UIImage *processedImage = [image proportionallyResizedImageToFitSize:CGSizeMake(160.f, 160.f)];
//    UIImage *processedImage = [image proportionallyResizedImageToFillSize:CGSizeMake(160.f, 160.f)];
    self.processedImageView.image = processedImage;
}

- (BOOL)shouldAutorotate {
    return NO;
}

@end
