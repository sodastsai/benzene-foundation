//
//  BDZoomImageViewController.m
//  Benzene Demo
//
//  BSD License
//
//  Copyright (c) 2012-2015, Wantoto Inc.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#import "BDZoomImageViewController.h"

@interface BDZoomImageViewController () {
    CGFloat hue;
}
@property (strong, nonatomic) IBOutlet UITextField *scaleField;
@property (strong, nonatomic) IBOutlet UIToolbar *scaleFieldToolbar;

- (IBAction)loadImage:(id)sender;
- (IBAction)visibleRange:(id)sender;
- (IBAction)scaleFieldDone:(id)sender;


@end

@implementation BDZoomImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.scaleField.inputAccessoryView = self.scaleFieldToolbar;
    self.zoomImageView.imageView.image = [UIImage imageNamed:@"wantoto_logo"];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.zoomImageView zoomToFitScaleWithAnimation:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self.zoomImageView zoomToFitScaleWithAnimation:YES];
}

#pragma mark - IBActions

- (IBAction)loadImage:(id)sender {
    UIColor *color = [UIColor colorWithHue:hue saturation:1.f brightness:1.f alpha:1.f];
    UIImage *image = [[UIImage imageNamed:@"wantoto_logo"] imageColoredWithColor:color];
    [self.zoomImageView setImage:image updateContentSize:NO];
    hue = hue>=1.f ? 0.f : (hue+.1f);
}

- (IBAction)visibleRange:(id)sender {
    NSLog(@"%@", [NSValue valueWithCGRect:[self.zoomImageView visibleRectOfImage]]);
}

- (IBAction)scaleFieldDone:(id)sender {
    [self.scaleField resignFirstResponder];
    
    CGFloat scale = [self.scaleField.text floatValue];
    [self.zoomImageView zoomToScale:scale atPoint:self.zoomImageView.center animated:YES];
    
    self.scaleField.text = @"";
}

@end
